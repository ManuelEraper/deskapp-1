﻿Public Class Main
    Dim seleccion As String
    Dim _listaReproduccion As New lista_reproduccion()
    Dim _album As New Album()
    Dim _artistaModif As New Artista()
    Dim _usuarioModif As New Usuario()
    Dim _cancion As New Cancion()
    Dim _genero As New Genero()


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        For Each Button In Me.Controls.OfType(Of Button)
            Button.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(90, Byte), Integer))
        Next
        artistaOpciones.Show()
        artistaOpciones.BringToFront()
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(884, 550)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        For Each Button In Me.Controls.OfType(Of Button)
            Button.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(90, Byte), Integer))
        Next
        albumOpciones.Show()
        albumOpciones.BringToFront()
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(884, 550)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        For Each Button In Me.Controls.OfType(Of Button)
            Button.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(90, Byte), Integer))
        Next
        cancionOpciones.Show()
        cancionOpciones.BringToFront()
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(884, 550)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        For Each Button In Me.Controls.OfType(Of Button)
            Button.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(90, Byte), Integer))
        Next
        generoOpciones.Show()
        generoOpciones.BringToFront()
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(884, 550)
    End Sub
    Private Sub subirArtistaClick(sender As Object, e As EventArgs) Handles bttnCrearArtista.Click
        subirArtista.Show()
        subirArtista.BringToFront()
    End Sub
    Private Sub eliminarArtistaClick(sender As Object, e As EventArgs) Handles bttnEliminarArtista.Click
        eliminarArtista.Show()
        eliminarArtista.BringToFront()
    End Sub
    Private Sub modificarArtistaClick(sender As Object, e As EventArgs) Handles bttnModifArtista.Click
        modifArtista1.Show()
        modifArtista1.BringToFront()
    End Sub
    Private Sub listarArtistasClick(sender As Object, e As EventArgs) Handles bttnListarArtistas.Click
        listarArtistas.Show()
        listarArtistas.BringToFront()
    End Sub
    Private Sub crearAlbumClick(sender As Object, e As EventArgs) Handles bttnCrearAlbum.Click
        subirAlbum.Show()
        subirAlbum.BringToFront()
    End Sub
    Private Sub modificarAlbumClick(sender As Object, e As EventArgs) Handles bttnModifAlbum.Click
        modifAlbum1.Show()
        modifAlbum1.BringToFront()
    End Sub
    Private Sub eliminarAlbumClick(sender As Object, e As EventArgs) Handles bttnEliminarAlbum.Click
        eliminarAlbum.Show()
        eliminarAlbum.BringToFront()
    End Sub
    Private Sub listarAlbumClick(sender As Object, e As EventArgs) Handles bttnListarAlbums.Click
        listarAlbums.Show()
        listarAlbums.BringToFront()
    End Sub
    Private Sub listarCancionesClick(sender As Object, e As EventArgs) Handles bttnListarCanciones.Click
        listarCanciones.Show()
        listarCanciones.BringToFront()
    End Sub
    Private Sub subirCancionClick(sender As Object, e As EventArgs) Handles bttnCrearCancion.Click
        subirCancion.Show()
        subirCancion.BringToFront()
        Me.ClientSize = New System.Drawing.Size(1025, 550)
    End Sub
    Private Sub btnSubidaArtista(sender As Object, e As EventArgs) Handles Button5.Click
        Dim _usuario As New Usuario()
        Dim _artista As New Artista()
        GlobalFunctions.subir(subirArtista, lblErrorSubirArtista, _usuario, _artista)
    End Sub

    Private Sub EliminarArtista_Paint(sender As Object, e As PaintEventArgs) Handles eliminarArtista.Paint
        lblErrorEliminarArtista.Text = ""
        GlobalFunctions.listarListView(lvArtistas, "SELECT usuario.id, usuario.nombre, usuario.email, artista.descripcion FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub BttnEliminar_Click(sender As Object, e As EventArgs) Handles bttnEliminar.Click
        GlobalFunctions.eliminar(lblErrorEliminarArtista, "DELETE FROM usuario WHERE id = " + lvArtistas.SelectedItems(0).Text)
        GlobalFunctions.listarListView(lvArtistas, "SELECT usuario.id, usuario.nombre, usuario.email, artista.descripcion FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub ListarArtistas_Paint(sender As Object, e As PaintEventArgs) Handles listarArtistas.Paint
        GlobalFunctions.listarListView(lvArtistasl, "SELECT usuario.id, usuario.nombre, usuario.email, artista.descripcion FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub LvArtistasModif1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvArtistasModif1.SelectedIndexChanged
        GlobalFunctions.listarListView(lvArtistasl, "SELECT usuario.id, usuario.nombre, usuario.email, artista.descripcion FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub BttnModif1_Click(sender As Object, e As EventArgs) Handles bttnModif1.Click
        seleccion = lvArtistasModif1.SelectedItems(0).SubItems(0).Text
        modifArtista2.Show()
        modifArtista2.BringToFront()
    End Sub

    Private Sub ModifArtista1_Paint(sender As Object, e As PaintEventArgs) Handles modifArtista1.Paint
        GlobalFunctions.listarListView(lvArtistasModif1, "SELECT usuario.id, usuario.nombre, usuario.email, artista.descripcion FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub ModifArtista2_Paint(sender As Object, e As PaintEventArgs) Handles modifArtista2.Paint
        lblErrorModifArtista2.Text = ""
        GlobalFunctions.mostrarModificado(modifArtista2, seleccion, _usuarioModif, _artistaModif)
    End Sub

    Private Sub SubirAlbum_Paint(sender As Object, e As PaintEventArgs)
        lblErrorSubirAlbum.Text = ""
        GlobalFunctions.listarListView(lvArtistasSubirAlbum, "SELECT usuario.id, usuario.nombre, usuario.email FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub ModifAlbum1_Paint(sender As Object, e As PaintEventArgs) Handles modifAlbum1.Paint
        GlobalFunctions.listarListView(lvArtistasModifAlbum1, "Select usuario.id, Usuario.nombre, Usuario.email, Artista.descripcion FROM usuario INNER JOIN artista On artista.id_usuario = usuario.id")
    End Sub

    Private Sub LvArtistasModifAlbum1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvArtistasModifAlbum1.SelectedIndexChanged
        If lvArtistasModifAlbum1.SelectedItems.Count > 0 Then
            GlobalFunctions.listarListView(lvAlbumsModifAlbum1, "Select lista_reproduccion.id, lista_reproduccion.nombre, lista_reproduccion.descripcion, lista_reproduccion.fecha_subida, Album.año_edicion FROM lista_reproduccion INNER JOIN album On album.id_lista_reproduccion = lista_reproduccion.id INNER JOIN usuario On usuario.id = album.id_usuario WHERE album.id_usuario = " + lvArtistasModifAlbum1.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        seleccion = lvAlbumsModifAlbum1.SelectedItems(0).Text
        For Each Panel As Panel In Me.Controls.OfType(Of Panel)
            Panel.Hide()
        Next
        modifAlbum2.Show()
    End Sub

    Private Sub Button9_Click_1(sender As Object, e As EventArgs) Handles Button9.Click
        GlobalFunctions.modificar(modifAlbum2, lblErrorModifAlbum2, _listaReproduccion, _album)
    End Sub

    Private Sub ModifAlbum2_Paint(sender As Object, e As PaintEventArgs) Handles modifAlbum2.Paint
        lblErrorModifAlbum2.Text = ""
        GlobalFunctions.mostrarModificado(modifAlbum2, seleccion, _listaReproduccion, _album)
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Dim xmlDate As String = Now.ToString("MM-dd-yyyy")
        _listaReproduccion.fecha_subida = xmlDate
        _album.id_usuario = lvArtistasSubirAlbum.SelectedItems(0).Text
        GlobalFunctions.subir(subirAlbum, lblErrorSubirAlbum, _listaReproduccion, _album)
    End Sub

    Private Sub EliminarAlbum_Paint(sender As Object, e As PaintEventArgs) Handles eliminarAlbum.Paint
        lblErrorEliminarAlbum.Text = ""
        GlobalFunctions.listarListView(lvArtistasEliminarAlbum, "Select id, Nombre, email, Artista.descripcion FROM usuario INNER JOIN artista On usuario.id = artista.id_usuario")
    End Sub

    Private Sub LvArtistasEliminarAlbum_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvArtistasEliminarAlbum.SelectedIndexChanged
        If lvArtistasEliminarAlbum.SelectedItems.Count > 0 Then
            GlobalFunctions.listarListView(lvAlbumsEliminarAlbum, "Select lista_reproduccion.id, lista_reproduccion.nombre, lista_reproduccion.descripcion, lista_reproduccion.fecha_subida, Album.año_edicion FROM lista_reproduccion INNER JOIN album On album.id_lista_reproduccion = lista_reproduccion.id INNER JOIN usuario On usuario.id = album.id_usuario WHERE album.id_usuario = " + lvArtistasEliminarAlbum.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        GlobalFunctions.eliminar(lblErrorEliminarAlbum, "DELETE FROM lista_reproduccion WHERE id = " + lvAlbumsEliminarAlbum.SelectedItems(0).Text)
        GlobalFunctions.listarListView(lvAlbumsEliminarAlbum, "Select lista_reproduccion.id, lista_reproduccion.nombre, lista_reproduccion.fecha_subida, lista_reproduccion.descripcion, Album.año_edicion FROM lista_reproduccion INNER JOIN album On album.id_lista_reproduccion = lista_reproduccion.id INNER JOIN usuario On usuario.id = album.id_usuario WHERE album.id_usuario = " + lvArtistasEliminarAlbum.SelectedItems(0).Text)
    End Sub

    Private Sub listarAlbums_Paint(sender As Object, e As PaintEventArgs) Handles listarAlbums.Paint
        GlobalFunctions.listarListView(lvAlbumsListarAlbums, "Select lista_reproduccion.id, Usuario.nombre, lista_reproduccion.nombre, lista_reproduccion.descripcion, lista_reproduccion.fecha_subida, Album.año_edicion FROM lista_reproduccion INNER JOIN album On album.id_lista_reproduccion = lista_reproduccion.id INNER JOIN usuario On usuario.id = album.id_usuario INNER JOIN artista On album.id_usuario = artista.id_usuario")
    End Sub

    Private Sub SubirCancion_Paint(sender As Object, e As PaintEventArgs) Handles subirCancion.Paint
        lblErrorSubirCancion.Text = ""
        GlobalFunctions.listarListBox(lbGenerosSubirCancion, "SELECT nombre FROM genero")
        GlobalFunctions.listarListView(lvArtistasSubirCancion, "SELECT usuario.id, usuario.nombre, usuario.email FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub LvArtistasSubirCancion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvArtistasSubirCancion.SelectedIndexChanged
        If lvArtistasSubirCancion.SelectedItems.Count > 0 Then
            GlobalFunctions.listarListView(lvAlbumsSubirCancion, "Select lista_reproduccion.id, lista_reproduccion.nombre, Album.año_edicion FROM lista_reproduccion INNER JOIN album ON lista_reproduccion.id = album.id_lista_reproduccion INNER JOIN artista on album.id_usuario = artista.id_usuario INNER JOIN usuario ON artista.id_usuario = usuario.id WHERE usuario.email = '" + lvArtistasSubirCancion.SelectedItems(0).SubItems(2).Text + "'")
        End If
    End Sub

    Private Sub bttnModifArtista2_Click(sender As Object, e As EventArgs) Handles bttnModifArtista2.Click
        GlobalFunctions.modificar(modifArtista2, lblErrorModifArtista2, _usuarioModif, _artistaModif)
    End Sub

    Private Sub subirAlbum_Paint_1(sender As Object, e As PaintEventArgs) Handles subirAlbum.Paint
        lblErrorSubirAlbum.Text = ""
        GlobalFunctions.listarListView(lvArtistasSubirAlbum, "SELECT usuario.id, usuario.nombre, usuario.email FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click
        _cancion.id_usuario = Convert.ToInt32(lvArtistasSubirCancion.SelectedItems.Item(0).Text)

        Dim _cancion_album As New album_cancion(_cancion.id_usuario, Nothing, Convert.ToInt32(lvAlbumsSubirCancion.SelectedItems.Item(0).Text))

        GlobalFunctions.subir(subirCancion, lblErrorSubirCancion, _cancion, _cancion_album)

        For Each genero As String In lbGenerosSubirCancion.SelectedItems
            Dim _canciongenero As New Cancion_genero(Nothing, _cancion.id_usuario, genero)
            GlobalFunctions.subir(subirCancion, lblErrorSubirCancion, _canciongenero)
        Next
    End Sub

    Private Sub listarCanciones_Paint(sender As Object, e As PaintEventArgs) Handles listarCanciones.Paint
        GlobalFunctions.listarListView(lvCancionesListarCanciones, "SELECT cancion.id, usuario.nombre, lista_reproduccion.nombre, cancion.nombre, cancion.link_imagen, cancion.link_recurso FROM cancion INNER JOIN artista ON cancion.id_usuario = artista.id_usuario INNER JOIN usuario ON artista.id_usuario = usuario.id INNER JOIN album_cancion ON album_cancion.id_cancion = cancion.id INNER JOIN lista_reproduccion ON album_cancion.id_lista_reproduccion = lista_reproduccion.id")
    End Sub

    Private Sub eliminarCancion_Paint(sender As Object, e As PaintEventArgs) Handles eliminarCancion.Paint
        lblErrorEliminarCancion.Text = ""
        GlobalFunctions.listarListView(lvArtistasEliminarCancion, "SELECT usuario.id, usuario.nombre, usuario.email FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub lvArtistasEliminarCanciona_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvArtistasEliminarCancion.SelectedIndexChanged
        If lvArtistasEliminarCancion.SelectedItems.Count > 0 Then
            GlobalFunctions.listarListView(lvAlbumsEliminarCancion, "Select lista_reproduccion.id, lista_reproduccion.nombre, lista_reproduccion.fecha_subida, lista_reproduccion.descripcion, album.año_edicion FROM lista_reproduccion INNER JOIN album ON album.id_lista_reproduccion = lista_reproduccion.id INNER JOIN usuario ON usuario.id = album.id_usuario WHERE album.id_usuario = " + lvArtistasEliminarCancion.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        GlobalFunctions.eliminar(lblErrorEliminarCancion, "DELETE FROM cancion WHERE id = " + lvCancionesEliminarCancion.SelectedItems(0).Text)
        GlobalFunctions.listarListView(lvCancionesEliminarCancion, "select cancion.id, cancion.nombre, cancion.link_imagen, cancion.link_recurso from cancion INNER JOIN album_cancion ON album_cancion.id_cancion = cancion.id INNER JOIN usuario ON album_cancion.id_usuario = usuario.id WHERE usuario.id = " + lvArtistasEliminarCancion.SelectedItems(0).Text + " and id_lista_reproduccion = " + lvAlbumsEliminarCancion.SelectedItems(0).Text)
    End Sub

    Private Sub bttnEliminarCancion_Click(sender As Object, e As EventArgs)
        eliminarCancion.Show()
        eliminarCancion.BringToFront()
        Me.ClientSize = New System.Drawing.Size(1025, 550)
    End Sub

    Private Sub lvAlbumsEliminarCancion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvAlbumsEliminarCancion.SelectedIndexChanged
        If lvAlbumsEliminarCancion.SelectedItems.Count > 0 Then
            GlobalFunctions.listarListView(lvCancionesEliminarCancion, "select cancion.id, cancion.nombre, cancion.link_imagen, cancion.link_recurso from cancion INNER JOIN album_cancion ON album_cancion.id_cancion = cancion.id INNER JOIN usuario ON album_cancion.id_usuario = usuario.id WHERE usuario.id = " + lvArtistasEliminarCancion.SelectedItems(0).Text + " and id_lista_reproduccion = " + lvAlbumsEliminarCancion.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub bttnEliminarCancion_Click_1(sender As Object, e As EventArgs) Handles bttnEliminarCancion.Click
        eliminarCancion.Show()
        eliminarCancion.BringToFront()
        Me.ClientSize = New System.Drawing.Size(1025, 550)
    End Sub

    Private Sub lvArtistasModifCancion1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvArtistasModifCancion1.SelectedIndexChanged
        If lvArtistasModifCancion1.SelectedItems.Count > 0 Then
            GlobalFunctions.listarListView(lvAlbumsModifCancion1, "Select lista_reproduccion.id, lista_reproduccion.nombre, lista_reproduccion.fecha_subida, lista_reproduccion.descripcion, album.año_edicion FROM lista_reproduccion INNER JOIN album ON album.id_lista_reproduccion = lista_reproduccion.id INNER JOIN usuario ON usuario.id = album.id_usuario WHERE album.id_usuario = " + lvArtistasModifCancion1.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub lvAlbumsModifCancion1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvAlbumsModifCancion1.SelectedIndexChanged
        If lvAlbumsModifCancion1.SelectedItems.Count > 0 Then
            GlobalFunctions.listarListView(lvCancionesModifCancion1, "select cancion.id, cancion.nombre, cancion.link_imagen, cancion.link_recurso from cancion INNER JOIN album_cancion ON album_cancion.id_cancion = cancion.id INNER JOIN usuario ON album_cancion.id_usuario = usuario.id WHERE usuario.id = " + lvArtistasModifCancion1.SelectedItems(0).Text + " and id_lista_reproduccion = " + lvAlbumsModifCancion1.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        seleccion = lvCancionesModifCancion1.SelectedItems(0).Text
        For Each Panel As Panel In Me.Controls.OfType(Of Panel)
            Panel.Hide()
        Next
        modifCancion2.Show()
        Me.ClientSize = New System.Drawing.Size(899, 550)
    End Sub

    Private Sub modifCancion2_Paint(sender As Object, e As PaintEventArgs) Handles modifCancion2.Paint
        lblErrorModifCancion2.Text = ""

        Dim clasecnn = New ConnectionPgSql
        Dim conexion As New Npgsql.NpgsqlConnection()
        conexion = clasecnn.AbrirConexion()
        Dim cmd = New Npgsql.NpgsqlCommand()
        cmd.Connection = conexion
        Dim lector As Npgsql.NpgsqlDataReader

        GlobalFunctions.mostrarModificado(modifCancion2, seleccion, _cancion)
        GlobalFunctions.listarListBox(lbGenerosModifCancion2, "SELECT nombre FROM genero")

        cmd.CommandText = "SELECT genero_nombre FROM cancion_genero WHERE id_usuario = " + lvArtistasModifCancion1.SelectedItems(0).Text + " and id_cancion = " + lvCancionesModifCancion1.SelectedItems.Item(0).Text
        lector = cmd.ExecuteReader()

        If lector.HasRows Then
            While lector.Read()
                lbGenerosModifCancion2.SelectedItems.Add(lector(0).ToString)
            End While
        End If
    End Sub

    Private Sub bttnModifCancion_Click(sender As Object, e As EventArgs) Handles bttnModifCancion.Click
        modifCancion1.Show()
        modifCancion1.BringToFront()
        Me.ClientSize = New System.Drawing.Size(1025, 550)
    End Sub

    Private Sub modifCancion1_Paint(sender As Object, e As PaintEventArgs) Handles modifCancion1.Paint
        GlobalFunctions.listarListView(lvArtistasModifCancion1, "SELECT usuario.id, usuario.nombre, usuario.email FROM usuario INNER JOIN artista ON artista.id_usuario = usuario.id")
    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        GlobalFunctions.modificar(modifCancion2, lblErrorModifCancion2, _cancion)
        GlobalFunctions.eliminar(lblErrorModifCancion2, "DELETE FROM cancion_genero WHERE id_usuario = " + _cancion.id_usuario.ToString + " AND id_cancion = " + _cancion.id.ToString + ";")
        For Each genero As String In lbGenerosModifCancion2.SelectedItems
            Dim _cancion_genero As New Cancion_genero(_cancion.id, _cancion.id_usuario, genero)
            GlobalFunctions.subir(modifCancion2, lblErrorModifCancion2, _cancion_genero)
        Next

    End Sub

    Private Sub subirGenero_Paint(sender As Object, e As PaintEventArgs) Handles subirGenero.Paint
        lblErrorSubirGenero.Text = ""
        GlobalFunctions.listarListBox(lbGenerosSubirGenero, "Select nombre FROM genero")
    End Sub

    Private Sub bttnCrearGenero_Click(sender As Object, e As EventArgs) Handles bttnCrearGenero.Click
        subirGenero.Show()
        subirGenero.BringToFront()
    End Sub
    Private Sub bttnEliminarGenero_Click(sender As Object, e As EventArgs) Handles bttnEliminarGenero.Click
        eliminarGenero.Show()
        eliminarGenero.BringToFront()
    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        GlobalFunctions.subir(subirGenero, lblErrorSubirGenero, _genero)
        GlobalFunctions.listarListBox(lbGenerosSubirGenero, "Select nombre FROM genero")
    End Sub

    Private Sub EliminarGenero_Paint(sender As Object, e As PaintEventArgs) Handles eliminarGenero.Paint
        lblErrorEliminarGenero.Text = ""
        GlobalFunctions.listarListBox(lbGenerosEliminarGenero, "SELECT * FROM genero")
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click
        GlobalFunctions.eliminar(lblErrorEliminarGenero, "DELETE FROM genero WHERE genero.nombre = '" + lbGenerosEliminarGenero.SelectedItem + "'")
        GlobalFunctions.listarListBox(lbGenerosEliminarGenero, "SELECT * FROM genero")
    End Sub
End Class