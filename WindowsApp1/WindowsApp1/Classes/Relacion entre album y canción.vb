﻿Public Class album_cancion
    Public _id_usuario As Integer
    Public _id_cancion As Integer
    Public _id_lista_reproduccion As Integer
    Public Function traducir(PropertyInfo) As String
        Select Case PropertyInfo.Name
            Case "id_usuario"
                Return "ID del Usuario"
            Case "id_cancion"
                Return "ID de la canción"
            Case "id_lista_reproduccion"
                Return "ID del album"
        End Select
    End Function

    Public Property id_usuario() As Integer
        Get
            Return _id_usuario
        End Get
        Set(value As Integer)
            _id_usuario = value
        End Set
    End Property
    Public Property id_cancion() As Integer
        Get
            Return _id_cancion
        End Get
        Set(value As Integer)
            _id_cancion = value
        End Set
    End Property
    Public Property id_lista_reproduccion() As Integer
        Get
            Return _id_lista_reproduccion
        End Get
        Set(value As Integer)
            _id_lista_reproduccion = value
        End Set
    End Property
    Public Sub New(iduser As Integer, idcanc As Integer, idlist As Integer)
        id_usuario = iduser
        id_cancion = idcanc
        id_lista_reproduccion = idlist
    End Sub
End Class
