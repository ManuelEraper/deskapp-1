﻿Public Class lista_reproduccion
    Private _nombre As String
    Private _fecha_subida As String
    Private _linkImagen As String
    Private _descripcion As String
    Private _id As Integer
    Public Function traducir(PropertyInfo) As String
        Select Case PropertyInfo.Name
            Case "Nombre"
                Return "Nombre del album"
            Case "fecha_subida"
                Return "Fecha de subida"
            Case "link_imagen"
                Return "Link de la imagen"
            Case "descripcion"
                Return "Descripción del album"
            Case "id"
                Return "ID del album"
        End Select
    End Function
    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property
    Public Property fecha_subida() As String
        Get
            Return _fecha_subida
        End Get
        Set(value As String)
            _fecha_subida = value
        End Set
    End Property
    Public Property link_imagen() As String
        Get
            Return _linkImagen
        End Get
        Set(value As String)
            _linkImagen = value
        End Set
    End Property
    Public Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property
    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Public Sub New(nom As String, subida As Date, link As String, descr As String, idl As Integer)
        nombre = nom
        fecha_subida = subida
        link_imagen = link
        descripcion = descr
        id = idl
    End Sub

    Public Sub New()
    End Sub
End Class
