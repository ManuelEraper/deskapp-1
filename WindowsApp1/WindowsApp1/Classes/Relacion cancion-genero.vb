﻿Public Class Cancion_genero
    Public _id_usuario As Integer
    Public _id_cancion As Integer
    Public _genero_nombre As String
    Public Function traducir(PropertyInfo) As String
        Select Case PropertyInfo.Name
            Case "id_cancion"
                Return "ID de la canción"
            Case "id_usuario"
                Return "ID del Usuario"
            Case "genero_nombre"
                Return "Nombre del género"
        End Select
    End Function
    Public Property id_usuario() As Integer
        Get
            Return _id_usuario
        End Get
        Set(value As Integer)
            _id_usuario = value
        End Set
    End Property
    Public Property id_cancion() As Integer
        Get
            Return _id_cancion
        End Get
        Set(value As Integer)
            _id_cancion = value
        End Set
    End Property
    Public Property genero_nombre() As String
        Get
            Return _genero_nombre
        End Get
        Set(value As String)
            _genero_nombre = value
        End Set
    End Property
    Public Sub New(idc As Integer, idu As Integer, nom As String)
        id_cancion = idc
        id_usuario = idu
        genero_nombre = nom
    End Sub

    Public Sub New()
    End Sub
End Class
