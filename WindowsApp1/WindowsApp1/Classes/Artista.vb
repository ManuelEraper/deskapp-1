﻿
Public Class Artista
    Private _descripcion As String
    Private _id_usuario As Integer
    Public Function traducir(PropertyInfo) As String
        Select Case PropertyInfo.Name
            Case "descripcion"
                Return "Descripción del artista"
            Case "id_usuario"
                Return "ID del Usuario"
        End Select
    End Function
    Public Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property
    Public Property id_usuario() As Integer
        Get
            Return _id_usuario
        End Get
        Set(value As Integer)
            _id_usuario = value
        End Set
    End Property
    Public Sub New(desc As String, idUser As Integer)
        descripcion = desc
        id_usuario = idUser
    End Sub

    Public Sub New()
    End Sub
End Class
