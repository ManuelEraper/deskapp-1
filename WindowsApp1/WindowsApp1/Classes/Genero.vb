﻿Public Class Genero

    Dim _nombre As String
    Public Function traducir(PropertyInfo) As String
        Select Case PropertyInfo.Name
            Case "Nombre"
                Return "Nombre del género"
        End Select
    End Function
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Sub New(nom As String)
        Nombre = nom
    End Sub

    Public Sub New()
    End Sub
End Class
