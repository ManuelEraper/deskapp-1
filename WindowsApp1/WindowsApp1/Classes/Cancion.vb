﻿Public Class Cancion
    Dim _nombre As String
    Dim _linkImagen As String
    Dim _linkCancion As String
    Dim _id_usuario As Integer
    Dim _id As Integer

    Public Function traducir(PropertyInfo) As String
        Select Case PropertyInfo.Name
            Case "Nombre"
                Return "Nombre de la canción"
            Case "link_imagen"
                Return "Link de la imagen"
            Case "link_recurso"
                Return "Link de la canción"
            Case "id_usuario"
                Return "ID del usuario"
        End Select
    End Function
    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property
    Public Property link_imagen() As String
        Get
            Return _linkImagen
        End Get
        Set(value As String)
            _linkImagen = value
        End Set
    End Property
    Public Property link_recurso() As String
        Get
            Return _linkCancion
        End Get
        Set(value As String)
            _linkCancion = value
        End Set
    End Property
    Public Property id_usuario() As Integer
        Get
            Return _id_usuario
        End Get
        Set(value As Integer)
            _id_usuario = value
        End Set
    End Property
    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property
    Public Sub New(nom As String, imagen As String, cancion As String, idArt As Integer)
        nombre = nom
        link_imagen = imagen
        link_recurso = cancion
        id_usuario = idArt
    End Sub

    Public Sub New()
    End Sub
End Class

