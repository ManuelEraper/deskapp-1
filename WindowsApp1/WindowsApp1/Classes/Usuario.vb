﻿Public Class Usuario
    Private _nombre As String
    Private _email As String
    Private _linkImagen As String
    Private _id As Integer
    Public Function traducir(PropertyInfo) As String
        Select Case PropertyInfo.Name
            Case "nombre"
                Return "Nombre"
            Case "email"
                Return "Correo"
            Case "link_imagen"
                Return "Link de la imagen"
            Case "id"
                Return "ID"
        End Select
    End Function
    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property
    Public Property email() As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property
    Public Property link_imagen() As String
        Get
            Return _linkImagen
        End Get
        Set(value As String)
            _linkImagen = value
        End Set
    End Property
    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property
    Public Sub New(nom As String, mail As String, link As String)
        nombre = nom
        email = mail
        link_imagen = link
    End Sub

    Public Sub New()
    End Sub
End Class
