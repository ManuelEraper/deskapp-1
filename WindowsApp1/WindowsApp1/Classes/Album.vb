﻿Public Class Album
    Private _añoEdicion As Integer
    Private _id_usuario As Integer
    Private _id_listaReproduccion As Integer

    Public Function traducir(PropertyInfo) As String
        Select Case PropertyInfo.Name
            Case "año_edicion"
                Return "Año de edición"
            Case "id_usuario"
                Return "ID del Usuario"
            Case "id_listaReproduccion"
                Return "ID de la lista de reproducción"
        End Select
    End Function
    Public Property año_edicion() As Integer
        Get
            Return _añoEdicion
        End Get
        Set(value As Integer)
            _añoEdicion = value
        End Set
    End Property
    Public Property id_usuario() As Integer
        Get
            Return _id_usuario
        End Get
        Set(value As Integer)
            _id_usuario = value
        End Set
    End Property
    Public Property id_lista_reproduccion() As Integer
        Get
            Return _id_listaReproduccion
        End Get
        Set(value As Integer)
            _id_listaReproduccion = value
        End Set
    End Property
    Public Sub New(año As Integer, idUser As Integer, idList As Integer)
        año_edicion = año
        id_usuario = idUser
        id_lista_reproduccion = idList
    End Sub

    Public Sub New()
    End Sub
End Class
