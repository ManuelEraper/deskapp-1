﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.artistaOpciones = New System.Windows.Forms.Panel()
        Me.bttnListarArtistas = New System.Windows.Forms.Button()
        Me.bttnEliminarArtista = New System.Windows.Forms.Button()
        Me.bttnModifArtista = New System.Windows.Forms.Button()
        Me.bttnCrearArtista = New System.Windows.Forms.Button()
        Me.lblSelección = New System.Windows.Forms.Label()
        Me.albumOpciones = New System.Windows.Forms.Panel()
        Me.bttnListarAlbums = New System.Windows.Forms.Button()
        Me.bttnEliminarAlbum = New System.Windows.Forms.Button()
        Me.bttnModifAlbum = New System.Windows.Forms.Button()
        Me.bttnCrearAlbum = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader16 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.ListView3 = New System.Windows.Forms.ListView()
        Me.ColumnHeader17 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader18 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader19 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader20 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cancionOpciones = New System.Windows.Forms.Panel()
        Me.bttnListarCanciones = New System.Windows.Forms.Button()
        Me.bttnEliminarCancion = New System.Windows.Forms.Button()
        Me.bttnModifCancion = New System.Windows.Forms.Button()
        Me.bttnCrearCancion = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.generoOpciones = New System.Windows.Forms.Panel()
        Me.bttnEliminarGenero = New System.Windows.Forms.Button()
        Me.bttnCrearGenero = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.subirArtista = New System.Windows.Forms.Panel()
        Me.lblErrorSubirArtista = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtDescripcionSubirArtista = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtlink_imagenSubirArtista = New System.Windows.Forms.TextBox()
        Me.txtEmailSubirArtista = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNombreSubirArtista = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.eliminarArtista = New System.Windows.Forms.Panel()
        Me.lvArtistas = New System.Windows.Forms.ListView()
        Me.ID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Nombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Correo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Descripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblErrorEliminarArtista = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.bttnEliminar = New System.Windows.Forms.Button()
        Me.listarArtistas = New System.Windows.Forms.Panel()
        Me.lvArtistasl = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label12 = New System.Windows.Forms.Label()
        Me.ListView4 = New System.Windows.Forms.ListView()
        Me.ColumnHeader33 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader34 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader35 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader36 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label39 = New System.Windows.Forms.Label()
        Me.modifArtista1 = New System.Windows.Forms.Panel()
        Me.lvArtistasModif1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblErrorModifArtista1 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.bttnModif1 = New System.Windows.Forms.Button()
        Me.modifArtista2 = New System.Windows.Forms.Panel()
        Me.lblErrorModifArtista2 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtdescripcionModifArtista2 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtlink_imagenModifArtista2 = New System.Windows.Forms.TextBox()
        Me.txtemailModifArtista2 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtnombreModifArtista2 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.bttnModifArtista2 = New System.Windows.Forms.Button()
        Me.modifAlbum1 = New System.Windows.Forms.Panel()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.lvAlbumsModifAlbum1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader28 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader29 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader30 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader31 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader32 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvArtistasModifAlbum1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader24 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader25 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader26 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader27 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.modifAlbum2 = New System.Windows.Forms.Panel()
        Me.lblErrorModifAlbum2 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtDescripcionModifAlbum2 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtlink_imagenModifAlbum2 = New System.Windows.Forms.TextBox()
        Me.txtaño_edicionmodifAlbum2 = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtnombreModifAlbum2 = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.subirAlbum = New System.Windows.Forms.Panel()
        Me.lvArtistasSubirAlbum = New System.Windows.Forms.ListView()
        Me.idar = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader21 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader22 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label45 = New System.Windows.Forms.Label()
        Me.lblErrorSubirAlbum = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtdescripcionSubirAlbum = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtlink_imagenSubirAlbum = New System.Windows.Forms.TextBox()
        Me.txtaño_edicionSubirAlbum = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.txtnombreSubirAlbum = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.eliminarAlbum = New System.Windows.Forms.Panel()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.lvAlbumsEliminarAlbum = New System.Windows.Forms.ListView()
        Me.ColumnHeader23 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader37 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader38 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader39 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader40 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvArtistasEliminarAlbum = New System.Windows.Forms.ListView()
        Me.ColumnHeader41 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader42 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader43 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader44 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblErrorEliminarAlbum = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.lvAlbums = New System.Windows.Forms.ListView()
        Me.ColumnHeader45 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Artista = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader46 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Año = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader47 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Subido = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.listarAlbums = New System.Windows.Forms.Panel()
        Me.lvAlbumsListarAlbums = New System.Windows.Forms.ListView()
        Me.ColumnHeader55 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader56 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader61 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader62 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader63 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader64 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.subirCancion = New System.Windows.Forms.Panel()
        Me.txtlink_recursoSubirCancion = New System.Windows.Forms.TextBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.txtlink_imagenSubirCancion = New System.Windows.Forms.TextBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.txtNombreSubirCancion = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.lbGenerosSubirCancion = New System.Windows.Forms.ListBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.lvAlbumsSubirCancion = New System.Windows.Forms.ListView()
        Me.ColumnHeader53 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader54 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader57 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvArtistasSubirCancion = New System.Windows.Forms.ListView()
        Me.ColumnHeader58 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader59 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader60 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblErrorSubirCancion = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.listarCanciones = New System.Windows.Forms.Panel()
        Me.lvCancionesListarCanciones = New System.Windows.Forms.ListView()
        Me.ColumnHeader51 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader52 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader65 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader68 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader69 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader70 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.eliminarCancion = New System.Windows.Forms.Panel()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.lvCancionesEliminarCancion = New System.Windows.Forms.ListView()
        Me.ColumnHeader72 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader75 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader76 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader77 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.lvAlbumsEliminarCancion = New System.Windows.Forms.ListView()
        Me.ColumnHeader48 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader49 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader50 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvArtistasEliminarCancion = New System.Windows.Forms.ListView()
        Me.ColumnHeader66 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader67 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader71 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblErrorEliminarCancion = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.modifCancion1 = New System.Windows.Forms.Panel()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.lvCancionesModifCancion1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader73 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader74 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader78 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader79 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.lvAlbumsModifCancion1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader80 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader81 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader82 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvArtistasModifCancion1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader83 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader84 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader85 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblErrorModifCancion1 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.modifCancion2 = New System.Windows.Forms.Panel()
        Me.lbGenerosModifCancion2 = New System.Windows.Forms.ListBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.lblErrorModifCancion2 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.txtnombreModifCancion2 = New System.Windows.Forms.TextBox()
        Me.txtlink_recursoModifCancion2 = New System.Windows.Forms.TextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.txtlink_imagenModifCancion2 = New System.Windows.Forms.TextBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.subirGenero = New System.Windows.Forms.Panel()
        Me.lbGenerosSubirGenero = New System.Windows.Forms.ListBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.lblErrorSubirGenero = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.txtnombreSubirGenero = New System.Windows.Forms.TextBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.eliminarGenero = New System.Windows.Forms.Panel()
        Me.lbGenerosEliminarGenero = New System.Windows.Forms.ListBox()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.lblErrorEliminarGenero = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Button23 = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.artistaOpciones.SuspendLayout()
        Me.albumOpciones.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.cancionOpciones.SuspendLayout()
        Me.generoOpciones.SuspendLayout()
        Me.subirArtista.SuspendLayout()
        Me.eliminarArtista.SuspendLayout()
        Me.listarArtistas.SuspendLayout()
        Me.modifArtista1.SuspendLayout()
        Me.modifArtista2.SuspendLayout()
        Me.modifAlbum1.SuspendLayout()
        Me.modifAlbum2.SuspendLayout()
        Me.subirAlbum.SuspendLayout()
        Me.eliminarAlbum.SuspendLayout()
        Me.listarAlbums.SuspendLayout()
        Me.subirCancion.SuspendLayout()
        Me.listarCanciones.SuspendLayout()
        Me.eliminarCancion.SuspendLayout()
        Me.modifCancion1.SuspendLayout()
        Me.modifCancion2.SuspendLayout()
        Me.subirGenero.SuspendLayout()
        Me.eliminarGenero.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.Button4.FlatAppearance.BorderSize = 0
        Me.Button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft JhengHei", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Image = Global.WindowsApp1.My.Resources.Resources.Webp2
        Me.Button4.Location = New System.Drawing.Point(0, 440)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(239, 110)
        Me.Button4.TabIndex = 19
        Me.Button4.Text = "Genero"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft JhengHei", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Image = Global.WindowsApp1.My.Resources.Resources.Webp_net_resizeimage__3_
        Me.Button3.Location = New System.Drawing.Point(0, 330)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(239, 110)
        Me.Button3.TabIndex = 18
        Me.Button3.Text = "Canción"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft JhengHei", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Image = Global.WindowsApp1.My.Resources.Resources.Webp1
        Me.Button2.Location = New System.Drawing.Point(0, 220)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(239, 110)
        Me.Button2.TabIndex = 17
        Me.Button2.Text = "Album"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft JhengHei", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Image = Global.WindowsApp1.My.Resources.Resources.Webp_net_resizeimage__5_
        Me.Button1.Location = New System.Drawing.Point(0, 110)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(239, 110)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "Artista"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button1.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.PictureBox1.Image = Global.WindowsApp1.My.Resources.Resources.logo__1_
        Me.PictureBox1.InitialImage = Global.WindowsApp1.My.Resources.Resources.negro_sombra
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(239, 110)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 15
        Me.PictureBox1.TabStop = False
        '
        'artistaOpciones
        '
        Me.artistaOpciones.Controls.Add(Me.bttnListarArtistas)
        Me.artistaOpciones.Controls.Add(Me.bttnEliminarArtista)
        Me.artistaOpciones.Controls.Add(Me.bttnModifArtista)
        Me.artistaOpciones.Controls.Add(Me.bttnCrearArtista)
        Me.artistaOpciones.Controls.Add(Me.lblSelección)
        Me.artistaOpciones.Location = New System.Drawing.Point(238, 0)
        Me.artistaOpciones.Name = "artistaOpciones"
        Me.artistaOpciones.Size = New System.Drawing.Size(646, 550)
        Me.artistaOpciones.TabIndex = 20
        Me.artistaOpciones.Visible = False
        '
        'bttnListarArtistas
        '
        Me.bttnListarArtistas.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnListarArtistas.FlatAppearance.BorderSize = 0
        Me.bttnListarArtistas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnListarArtistas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnListarArtistas.ForeColor = System.Drawing.Color.White
        Me.bttnListarArtistas.Location = New System.Drawing.Point(112, 399)
        Me.bttnListarArtistas.Name = "bttnListarArtistas"
        Me.bttnListarArtistas.Size = New System.Drawing.Size(438, 34)
        Me.bttnListarArtistas.TabIndex = 21
        Me.bttnListarArtistas.Text = "Listar artistas"
        Me.bttnListarArtistas.UseVisualStyleBackColor = False
        '
        'bttnEliminarArtista
        '
        Me.bttnEliminarArtista.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnEliminarArtista.FlatAppearance.BorderSize = 0
        Me.bttnEliminarArtista.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnEliminarArtista.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnEliminarArtista.ForeColor = System.Drawing.Color.White
        Me.bttnEliminarArtista.Location = New System.Drawing.Point(112, 319)
        Me.bttnEliminarArtista.Name = "bttnEliminarArtista"
        Me.bttnEliminarArtista.Size = New System.Drawing.Size(438, 34)
        Me.bttnEliminarArtista.TabIndex = 20
        Me.bttnEliminarArtista.Text = "Eliminar un artista"
        Me.bttnEliminarArtista.UseVisualStyleBackColor = False
        '
        'bttnModifArtista
        '
        Me.bttnModifArtista.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnModifArtista.FlatAppearance.BorderSize = 0
        Me.bttnModifArtista.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnModifArtista.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnModifArtista.ForeColor = System.Drawing.Color.White
        Me.bttnModifArtista.Location = New System.Drawing.Point(112, 239)
        Me.bttnModifArtista.Name = "bttnModifArtista"
        Me.bttnModifArtista.Size = New System.Drawing.Size(438, 34)
        Me.bttnModifArtista.TabIndex = 19
        Me.bttnModifArtista.Text = "Modificar un artista"
        Me.bttnModifArtista.UseVisualStyleBackColor = False
        '
        'bttnCrearArtista
        '
        Me.bttnCrearArtista.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnCrearArtista.FlatAppearance.BorderSize = 0
        Me.bttnCrearArtista.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnCrearArtista.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnCrearArtista.ForeColor = System.Drawing.Color.White
        Me.bttnCrearArtista.Location = New System.Drawing.Point(112, 159)
        Me.bttnCrearArtista.Name = "bttnCrearArtista"
        Me.bttnCrearArtista.Size = New System.Drawing.Size(438, 34)
        Me.bttnCrearArtista.TabIndex = 18
        Me.bttnCrearArtista.Text = "Crear un artista"
        Me.bttnCrearArtista.UseVisualStyleBackColor = False
        '
        'lblSelección
        '
        Me.lblSelección.AutoSize = True
        Me.lblSelección.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelección.ForeColor = System.Drawing.Color.White
        Me.lblSelección.Location = New System.Drawing.Point(184, 64)
        Me.lblSelección.Name = "lblSelección"
        Me.lblSelección.Size = New System.Drawing.Size(296, 34)
        Me.lblSelección.TabIndex = 17
        Me.lblSelección.Text = "Seleccione una opción"
        '
        'albumOpciones
        '
        Me.albumOpciones.Controls.Add(Me.bttnListarAlbums)
        Me.albumOpciones.Controls.Add(Me.bttnEliminarAlbum)
        Me.albumOpciones.Controls.Add(Me.bttnModifAlbum)
        Me.albumOpciones.Controls.Add(Me.bttnCrearAlbum)
        Me.albumOpciones.Controls.Add(Me.Label1)
        Me.albumOpciones.Controls.Add(Me.Panel1)
        Me.albumOpciones.Location = New System.Drawing.Point(238, 0)
        Me.albumOpciones.Name = "albumOpciones"
        Me.albumOpciones.Size = New System.Drawing.Size(646, 550)
        Me.albumOpciones.TabIndex = 22
        Me.albumOpciones.Visible = False
        '
        'bttnListarAlbums
        '
        Me.bttnListarAlbums.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnListarAlbums.FlatAppearance.BorderSize = 0
        Me.bttnListarAlbums.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnListarAlbums.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnListarAlbums.ForeColor = System.Drawing.Color.White
        Me.bttnListarAlbums.Location = New System.Drawing.Point(112, 399)
        Me.bttnListarAlbums.Name = "bttnListarAlbums"
        Me.bttnListarAlbums.Size = New System.Drawing.Size(438, 34)
        Me.bttnListarAlbums.TabIndex = 21
        Me.bttnListarAlbums.Text = "Listar albums"
        Me.bttnListarAlbums.UseVisualStyleBackColor = False
        '
        'bttnEliminarAlbum
        '
        Me.bttnEliminarAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnEliminarAlbum.FlatAppearance.BorderSize = 0
        Me.bttnEliminarAlbum.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnEliminarAlbum.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnEliminarAlbum.ForeColor = System.Drawing.Color.White
        Me.bttnEliminarAlbum.Location = New System.Drawing.Point(112, 319)
        Me.bttnEliminarAlbum.Name = "bttnEliminarAlbum"
        Me.bttnEliminarAlbum.Size = New System.Drawing.Size(438, 34)
        Me.bttnEliminarAlbum.TabIndex = 20
        Me.bttnEliminarAlbum.Text = "Eliminar un album"
        Me.bttnEliminarAlbum.UseVisualStyleBackColor = False
        '
        'bttnModifAlbum
        '
        Me.bttnModifAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnModifAlbum.FlatAppearance.BorderSize = 0
        Me.bttnModifAlbum.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnModifAlbum.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnModifAlbum.ForeColor = System.Drawing.Color.White
        Me.bttnModifAlbum.Location = New System.Drawing.Point(112, 239)
        Me.bttnModifAlbum.Name = "bttnModifAlbum"
        Me.bttnModifAlbum.Size = New System.Drawing.Size(438, 34)
        Me.bttnModifAlbum.TabIndex = 19
        Me.bttnModifAlbum.Text = "Modificar un album"
        Me.bttnModifAlbum.UseVisualStyleBackColor = False
        '
        'bttnCrearAlbum
        '
        Me.bttnCrearAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnCrearAlbum.FlatAppearance.BorderSize = 0
        Me.bttnCrearAlbum.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnCrearAlbum.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnCrearAlbum.ForeColor = System.Drawing.Color.White
        Me.bttnCrearAlbum.Location = New System.Drawing.Point(112, 159)
        Me.bttnCrearAlbum.Name = "bttnCrearAlbum"
        Me.bttnCrearAlbum.Size = New System.Drawing.Size(438, 34)
        Me.bttnCrearAlbum.TabIndex = 18
        Me.bttnCrearAlbum.Text = "Crear un album"
        Me.bttnCrearAlbum.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(184, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(296, 34)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Seleccione una opción"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Me.TextBox3)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Controls.Add(Me.TextBox4)
        Me.Panel1.Controls.Add(Me.Label24)
        Me.Panel1.Controls.Add(Me.Button8)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(8, 8)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(646, 550)
        Me.Panel1.TabIndex = 23
        Me.Panel1.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(264, 528)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(111, 17)
        Me.Label14.TabIndex = 74
        Me.Label14.Text = "Mensaje de error"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(218, 9)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(205, 34)
        Me.Label15.TabIndex = 73
        Me.Label15.Text = "Subir un artista"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(94, 256)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(82, 17)
        Me.Label16.TabIndex = 72
        Me.Label16.Text = "Descripción"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.ForeColor = System.Drawing.Color.White
        Me.TextBox1.Location = New System.Drawing.Point(97, 276)
        Me.TextBox1.MinimumSize = New System.Drawing.Size(2, 25)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(438, 184)
        Me.TextBox1.TabIndex = 71
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(94, 188)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(169, 17)
        Me.Label17.TabIndex = 70
        Me.Label17.Text = "Link de la imagen de perfil"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.ForeColor = System.Drawing.Color.White
        Me.TextBox2.Location = New System.Drawing.Point(97, 208)
        Me.TextBox2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(438, 20)
        Me.TextBox2.TabIndex = 69
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.ForeColor = System.Drawing.Color.White
        Me.TextBox3.Location = New System.Drawing.Point(97, 138)
        Me.TextBox3.MinimumSize = New System.Drawing.Size(2, 25)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(438, 20)
        Me.TextBox3.TabIndex = 67
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(94, 117)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(42, 17)
        Me.Label23.TabIndex = 66
        Me.Label23.Text = "Email"
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.ForeColor = System.Drawing.Color.White
        Me.TextBox4.Location = New System.Drawing.Point(97, 73)
        Me.TextBox4.MinimumSize = New System.Drawing.Size(2, 25)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(438, 20)
        Me.TextBox4.TabIndex = 65
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(94, 52)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(59, 17)
        Me.Label24.TabIndex = 64
        Me.Label24.Text = "Nombre"
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button8.FlatAppearance.BorderSize = 0
        Me.Button8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(97, 483)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(438, 34)
        Me.Button8.TabIndex = 21
        Me.Button8.Text = "Subir"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.ListView1)
        Me.Panel2.Location = New System.Drawing.Point(7, 90)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(646, 550)
        Me.Panel2.TabIndex = 76
        Me.Panel2.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(215, 9)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(244, 34)
        Me.Label18.TabIndex = 73
        Me.Label18.Text = "Listado de artistas"
        '
        'Panel3
        '
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(200, 100)
        Me.Panel3.TabIndex = 74
        '
        'ListView1
        '
        Me.ListView1.Location = New System.Drawing.Point(0, 0)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(121, 97)
        Me.ListView1.TabIndex = 75
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'ListView2
        '
        Me.ListView2.Location = New System.Drawing.Point(0, 0)
        Me.ListView2.Name = "ListView2"
        Me.ListView2.Size = New System.Drawing.Size(121, 97)
        Me.ListView2.TabIndex = 0
        Me.ListView2.UseCompatibleStateImageBehavior = False
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(0, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(100, 23)
        Me.Label19.TabIndex = 0
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(0, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(100, 23)
        Me.Label20.TabIndex = 0
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(0, 0)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(200, 100)
        Me.Panel4.TabIndex = 0
        '
        'ListView3
        '
        Me.ListView3.Location = New System.Drawing.Point(0, 0)
        Me.ListView3.Name = "ListView3"
        Me.ListView3.Size = New System.Drawing.Size(121, 97)
        Me.ListView3.TabIndex = 0
        Me.ListView3.UseCompatibleStateImageBehavior = False
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(0, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(100, 23)
        Me.Label21.TabIndex = 0
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(0, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(100, 23)
        Me.Label22.TabIndex = 0
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(0, 0)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 0
        '
        'cancionOpciones
        '
        Me.cancionOpciones.Controls.Add(Me.bttnListarCanciones)
        Me.cancionOpciones.Controls.Add(Me.bttnEliminarCancion)
        Me.cancionOpciones.Controls.Add(Me.bttnModifCancion)
        Me.cancionOpciones.Controls.Add(Me.bttnCrearCancion)
        Me.cancionOpciones.Controls.Add(Me.Label2)
        Me.cancionOpciones.Location = New System.Drawing.Point(238, 0)
        Me.cancionOpciones.Name = "cancionOpciones"
        Me.cancionOpciones.Size = New System.Drawing.Size(646, 550)
        Me.cancionOpciones.TabIndex = 23
        Me.cancionOpciones.Visible = False
        '
        'bttnListarCanciones
        '
        Me.bttnListarCanciones.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnListarCanciones.FlatAppearance.BorderSize = 0
        Me.bttnListarCanciones.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnListarCanciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnListarCanciones.ForeColor = System.Drawing.Color.White
        Me.bttnListarCanciones.Location = New System.Drawing.Point(112, 399)
        Me.bttnListarCanciones.Name = "bttnListarCanciones"
        Me.bttnListarCanciones.Size = New System.Drawing.Size(438, 34)
        Me.bttnListarCanciones.TabIndex = 21
        Me.bttnListarCanciones.Text = "Listar canciones"
        Me.bttnListarCanciones.UseVisualStyleBackColor = False
        '
        'bttnEliminarCancion
        '
        Me.bttnEliminarCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnEliminarCancion.FlatAppearance.BorderSize = 0
        Me.bttnEliminarCancion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnEliminarCancion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnEliminarCancion.ForeColor = System.Drawing.Color.White
        Me.bttnEliminarCancion.Location = New System.Drawing.Point(112, 319)
        Me.bttnEliminarCancion.Name = "bttnEliminarCancion"
        Me.bttnEliminarCancion.Size = New System.Drawing.Size(438, 34)
        Me.bttnEliminarCancion.TabIndex = 20
        Me.bttnEliminarCancion.Text = "Eliminar una canción"
        Me.bttnEliminarCancion.UseVisualStyleBackColor = False
        '
        'bttnModifCancion
        '
        Me.bttnModifCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnModifCancion.FlatAppearance.BorderSize = 0
        Me.bttnModifCancion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnModifCancion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnModifCancion.ForeColor = System.Drawing.Color.White
        Me.bttnModifCancion.Location = New System.Drawing.Point(112, 239)
        Me.bttnModifCancion.Name = "bttnModifCancion"
        Me.bttnModifCancion.Size = New System.Drawing.Size(438, 34)
        Me.bttnModifCancion.TabIndex = 19
        Me.bttnModifCancion.Text = "Modificar una canción"
        Me.bttnModifCancion.UseVisualStyleBackColor = False
        '
        'bttnCrearCancion
        '
        Me.bttnCrearCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnCrearCancion.FlatAppearance.BorderSize = 0
        Me.bttnCrearCancion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnCrearCancion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnCrearCancion.ForeColor = System.Drawing.Color.White
        Me.bttnCrearCancion.Location = New System.Drawing.Point(112, 159)
        Me.bttnCrearCancion.Name = "bttnCrearCancion"
        Me.bttnCrearCancion.Size = New System.Drawing.Size(438, 34)
        Me.bttnCrearCancion.TabIndex = 18
        Me.bttnCrearCancion.Text = "Crear una canción"
        Me.bttnCrearCancion.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(184, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(296, 34)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Seleccione una opción"
        '
        'generoOpciones
        '
        Me.generoOpciones.Controls.Add(Me.bttnEliminarGenero)
        Me.generoOpciones.Controls.Add(Me.bttnCrearGenero)
        Me.generoOpciones.Controls.Add(Me.Label3)
        Me.generoOpciones.Location = New System.Drawing.Point(238, 0)
        Me.generoOpciones.Name = "generoOpciones"
        Me.generoOpciones.Size = New System.Drawing.Size(646, 550)
        Me.generoOpciones.TabIndex = 22
        Me.generoOpciones.Visible = False
        '
        'bttnEliminarGenero
        '
        Me.bttnEliminarGenero.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnEliminarGenero.FlatAppearance.BorderSize = 0
        Me.bttnEliminarGenero.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnEliminarGenero.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnEliminarGenero.ForeColor = System.Drawing.Color.White
        Me.bttnEliminarGenero.Location = New System.Drawing.Point(112, 319)
        Me.bttnEliminarGenero.Name = "bttnEliminarGenero"
        Me.bttnEliminarGenero.Size = New System.Drawing.Size(438, 34)
        Me.bttnEliminarGenero.TabIndex = 20
        Me.bttnEliminarGenero.Text = "Eliminar un genero"
        Me.bttnEliminarGenero.UseVisualStyleBackColor = False
        '
        'bttnCrearGenero
        '
        Me.bttnCrearGenero.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnCrearGenero.FlatAppearance.BorderSize = 0
        Me.bttnCrearGenero.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnCrearGenero.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnCrearGenero.ForeColor = System.Drawing.Color.White
        Me.bttnCrearGenero.Location = New System.Drawing.Point(112, 239)
        Me.bttnCrearGenero.Name = "bttnCrearGenero"
        Me.bttnCrearGenero.Size = New System.Drawing.Size(438, 34)
        Me.bttnCrearGenero.TabIndex = 18
        Me.bttnCrearGenero.Text = "Crear un genero"
        Me.bttnCrearGenero.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(184, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(296, 34)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Seleccione una opción"
        '
        'subirArtista
        '
        Me.subirArtista.Controls.Add(Me.lblErrorSubirArtista)
        Me.subirArtista.Controls.Add(Me.Label8)
        Me.subirArtista.Controls.Add(Me.Label7)
        Me.subirArtista.Controls.Add(Me.txtDescripcionSubirArtista)
        Me.subirArtista.Controls.Add(Me.Label6)
        Me.subirArtista.Controls.Add(Me.txtlink_imagenSubirArtista)
        Me.subirArtista.Controls.Add(Me.txtEmailSubirArtista)
        Me.subirArtista.Controls.Add(Me.Label5)
        Me.subirArtista.Controls.Add(Me.txtNombreSubirArtista)
        Me.subirArtista.Controls.Add(Me.Label4)
        Me.subirArtista.Controls.Add(Me.Button5)
        Me.subirArtista.Location = New System.Drawing.Point(238, 0)
        Me.subirArtista.Name = "subirArtista"
        Me.subirArtista.Size = New System.Drawing.Size(646, 550)
        Me.subirArtista.TabIndex = 22
        Me.subirArtista.Visible = False
        '
        'lblErrorSubirArtista
        '
        Me.lblErrorSubirArtista.AutoSize = True
        Me.lblErrorSubirArtista.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorSubirArtista.ForeColor = System.Drawing.Color.White
        Me.lblErrorSubirArtista.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorSubirArtista.Name = "lblErrorSubirArtista"
        Me.lblErrorSubirArtista.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorSubirArtista.TabIndex = 74
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(218, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(205, 34)
        Me.Label8.TabIndex = 73
        Me.Label8.Text = "Subir un artista"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(94, 256)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(82, 17)
        Me.Label7.TabIndex = 72
        Me.Label7.Text = "Descripción"
        '
        'txtDescripcionSubirArtista
        '
        Me.txtDescripcionSubirArtista.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtDescripcionSubirArtista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcionSubirArtista.ForeColor = System.Drawing.Color.White
        Me.txtDescripcionSubirArtista.Location = New System.Drawing.Point(97, 276)
        Me.txtDescripcionSubirArtista.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtDescripcionSubirArtista.Multiline = True
        Me.txtDescripcionSubirArtista.Name = "txtDescripcionSubirArtista"
        Me.txtDescripcionSubirArtista.Size = New System.Drawing.Size(438, 184)
        Me.txtDescripcionSubirArtista.TabIndex = 71
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(94, 188)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(169, 17)
        Me.Label6.TabIndex = 70
        Me.Label6.Text = "Link de la imagen de perfil"
        '
        'txtlink_imagenSubirArtista
        '
        Me.txtlink_imagenSubirArtista.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtlink_imagenSubirArtista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlink_imagenSubirArtista.ForeColor = System.Drawing.Color.White
        Me.txtlink_imagenSubirArtista.Location = New System.Drawing.Point(97, 208)
        Me.txtlink_imagenSubirArtista.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtlink_imagenSubirArtista.Name = "txtlink_imagenSubirArtista"
        Me.txtlink_imagenSubirArtista.Size = New System.Drawing.Size(438, 20)
        Me.txtlink_imagenSubirArtista.TabIndex = 69
        '
        'txtEmailSubirArtista
        '
        Me.txtEmailSubirArtista.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtEmailSubirArtista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmailSubirArtista.ForeColor = System.Drawing.Color.White
        Me.txtEmailSubirArtista.Location = New System.Drawing.Point(97, 138)
        Me.txtEmailSubirArtista.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtEmailSubirArtista.Name = "txtEmailSubirArtista"
        Me.txtEmailSubirArtista.Size = New System.Drawing.Size(438, 20)
        Me.txtEmailSubirArtista.TabIndex = 67
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(94, 117)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 17)
        Me.Label5.TabIndex = 66
        Me.Label5.Text = "Email"
        '
        'txtNombreSubirArtista
        '
        Me.txtNombreSubirArtista.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtNombreSubirArtista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombreSubirArtista.ForeColor = System.Drawing.Color.White
        Me.txtNombreSubirArtista.Location = New System.Drawing.Point(97, 73)
        Me.txtNombreSubirArtista.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtNombreSubirArtista.Name = "txtNombreSubirArtista"
        Me.txtNombreSubirArtista.Size = New System.Drawing.Size(438, 20)
        Me.txtNombreSubirArtista.TabIndex = 65
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(94, 52)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 17)
        Me.Label4.TabIndex = 64
        Me.Label4.Text = "Nombre"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button5.FlatAppearance.BorderSize = 0
        Me.Button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(97, 483)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(438, 34)
        Me.Button5.TabIndex = 21
        Me.Button5.Text = "Subir"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'eliminarArtista
        '
        Me.eliminarArtista.Controls.Add(Me.lvArtistas)
        Me.eliminarArtista.Controls.Add(Me.lblErrorEliminarArtista)
        Me.eliminarArtista.Controls.Add(Me.Label10)
        Me.eliminarArtista.Controls.Add(Me.bttnEliminar)
        Me.eliminarArtista.Location = New System.Drawing.Point(238, 0)
        Me.eliminarArtista.Name = "eliminarArtista"
        Me.eliminarArtista.Size = New System.Drawing.Size(646, 550)
        Me.eliminarArtista.TabIndex = 75
        Me.eliminarArtista.Visible = False
        '
        'lvArtistas
        '
        Me.lvArtistas.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvArtistas.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvArtistas.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ID, Me.Nombre, Me.Correo, Me.Descripcion})
        Me.lvArtistas.ForeColor = System.Drawing.Color.White
        Me.lvArtistas.FullRowSelect = True
        Me.lvArtistas.HideSelection = False
        Me.lvArtistas.Location = New System.Drawing.Point(49, 64)
        Me.lvArtistas.MultiSelect = False
        Me.lvArtistas.Name = "lvArtistas"
        Me.lvArtistas.Size = New System.Drawing.Size(564, 396)
        Me.lvArtistas.TabIndex = 75
        Me.lvArtistas.UseCompatibleStateImageBehavior = False
        Me.lvArtistas.View = System.Windows.Forms.View.Details
        '
        'ID
        '
        Me.ID.Text = "ID"
        Me.ID.Width = 39
        '
        'Nombre
        '
        Me.Nombre.Text = "Nombre"
        Me.Nombre.Width = 108
        '
        'Correo
        '
        Me.Correo.Text = "Email"
        Me.Correo.Width = 183
        '
        'Descripcion
        '
        Me.Descripcion.Text = "Descripción"
        Me.Descripcion.Width = 234
        '
        'lblErrorEliminarArtista
        '
        Me.lblErrorEliminarArtista.AutoSize = True
        Me.lblErrorEliminarArtista.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorEliminarArtista.ForeColor = System.Drawing.Color.White
        Me.lblErrorEliminarArtista.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorEliminarArtista.Name = "lblErrorEliminarArtista"
        Me.lblErrorEliminarArtista.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorEliminarArtista.TabIndex = 74
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(215, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(242, 34)
        Me.Label10.TabIndex = 73
        Me.Label10.Text = "Eliminar un artista"
        '
        'bttnEliminar
        '
        Me.bttnEliminar.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnEliminar.FlatAppearance.BorderSize = 0
        Me.bttnEliminar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnEliminar.ForeColor = System.Drawing.Color.White
        Me.bttnEliminar.Location = New System.Drawing.Point(97, 483)
        Me.bttnEliminar.Name = "bttnEliminar"
        Me.bttnEliminar.Size = New System.Drawing.Size(438, 34)
        Me.bttnEliminar.TabIndex = 21
        Me.bttnEliminar.Text = "Eliminar seleccionado"
        Me.bttnEliminar.UseVisualStyleBackColor = False
        '
        'listarArtistas
        '
        Me.listarArtistas.Controls.Add(Me.lvArtistasl)
        Me.listarArtistas.Controls.Add(Me.Label12)
        Me.listarArtistas.Location = New System.Drawing.Point(238, 0)
        Me.listarArtistas.Name = "listarArtistas"
        Me.listarArtistas.Size = New System.Drawing.Size(646, 550)
        Me.listarArtistas.TabIndex = 76
        Me.listarArtistas.Visible = False
        '
        'lvArtistasl
        '
        Me.lvArtistasl.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvArtistasl.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvArtistasl.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lvArtistasl.ForeColor = System.Drawing.Color.White
        Me.lvArtistasl.FullRowSelect = True
        Me.lvArtistasl.Location = New System.Drawing.Point(49, 64)
        Me.lvArtistasl.Name = "lvArtistasl"
        Me.lvArtistasl.Size = New System.Drawing.Size(564, 461)
        Me.lvArtistasl.TabIndex = 75
        Me.lvArtistasl.UseCompatibleStateImageBehavior = False
        Me.lvArtistasl.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ID"
        Me.ColumnHeader1.Width = 39
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Nombre"
        Me.ColumnHeader2.Width = 108
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Email"
        Me.ColumnHeader3.Width = 183
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Descripción"
        Me.ColumnHeader4.Width = 234
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(215, 9)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(244, 34)
        Me.Label12.TabIndex = 73
        Me.Label12.Text = "Listado de artistas"
        '
        'ListView4
        '
        Me.ListView4.Location = New System.Drawing.Point(0, 0)
        Me.ListView4.Name = "ListView4"
        Me.ListView4.Size = New System.Drawing.Size(121, 97)
        Me.ListView4.TabIndex = 0
        Me.ListView4.UseCompatibleStateImageBehavior = False
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(0, 0)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(100, 23)
        Me.Label39.TabIndex = 0
        '
        'modifArtista1
        '
        Me.modifArtista1.Controls.Add(Me.lvArtistasModif1)
        Me.modifArtista1.Controls.Add(Me.lblErrorModifArtista1)
        Me.modifArtista1.Controls.Add(Me.Label13)
        Me.modifArtista1.Controls.Add(Me.bttnModif1)
        Me.modifArtista1.Location = New System.Drawing.Point(238, 0)
        Me.modifArtista1.Name = "modifArtista1"
        Me.modifArtista1.Size = New System.Drawing.Size(646, 550)
        Me.modifArtista1.TabIndex = 76
        Me.modifArtista1.Visible = False
        '
        'lvArtistasModif1
        '
        Me.lvArtistasModif1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvArtistasModif1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvArtistasModif1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader8})
        Me.lvArtistasModif1.ForeColor = System.Drawing.Color.White
        Me.lvArtistasModif1.FullRowSelect = True
        Me.lvArtistasModif1.HideSelection = False
        Me.lvArtistasModif1.Location = New System.Drawing.Point(49, 64)
        Me.lvArtistasModif1.MultiSelect = False
        Me.lvArtistasModif1.Name = "lvArtistasModif1"
        Me.lvArtistasModif1.Size = New System.Drawing.Size(564, 396)
        Me.lvArtistasModif1.TabIndex = 75
        Me.lvArtistasModif1.UseCompatibleStateImageBehavior = False
        Me.lvArtistasModif1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "ID"
        Me.ColumnHeader5.Width = 39
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Nombre"
        Me.ColumnHeader6.Width = 108
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Email"
        Me.ColumnHeader7.Width = 183
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Descripción"
        Me.ColumnHeader8.Width = 234
        '
        'lblErrorModifArtista1
        '
        Me.lblErrorModifArtista1.AutoSize = True
        Me.lblErrorModifArtista1.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorModifArtista1.ForeColor = System.Drawing.Color.White
        Me.lblErrorModifArtista1.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorModifArtista1.Name = "lblErrorModifArtista1"
        Me.lblErrorModifArtista1.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorModifArtista1.TabIndex = 74
        Me.lblErrorModifArtista1.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(123, 17)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(412, 34)
        Me.Label13.TabIndex = 73
        Me.Label13.Text = "Seleccione el artista a modificar"
        '
        'bttnModif1
        '
        Me.bttnModif1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnModif1.FlatAppearance.BorderSize = 0
        Me.bttnModif1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnModif1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnModif1.ForeColor = System.Drawing.Color.White
        Me.bttnModif1.Location = New System.Drawing.Point(97, 483)
        Me.bttnModif1.Name = "bttnModif1"
        Me.bttnModif1.Size = New System.Drawing.Size(438, 34)
        Me.bttnModif1.TabIndex = 21
        Me.bttnModif1.Text = "Modificar seleccionado"
        Me.bttnModif1.UseVisualStyleBackColor = False
        '
        'modifArtista2
        '
        Me.modifArtista2.Controls.Add(Me.lblErrorModifArtista2)
        Me.modifArtista2.Controls.Add(Me.Label26)
        Me.modifArtista2.Controls.Add(Me.Label27)
        Me.modifArtista2.Controls.Add(Me.txtdescripcionModifArtista2)
        Me.modifArtista2.Controls.Add(Me.Label28)
        Me.modifArtista2.Controls.Add(Me.txtlink_imagenModifArtista2)
        Me.modifArtista2.Controls.Add(Me.txtemailModifArtista2)
        Me.modifArtista2.Controls.Add(Me.Label34)
        Me.modifArtista2.Controls.Add(Me.txtnombreModifArtista2)
        Me.modifArtista2.Controls.Add(Me.Label35)
        Me.modifArtista2.Controls.Add(Me.bttnModifArtista2)
        Me.modifArtista2.Location = New System.Drawing.Point(238, 0)
        Me.modifArtista2.Name = "modifArtista2"
        Me.modifArtista2.Size = New System.Drawing.Size(646, 550)
        Me.modifArtista2.TabIndex = 78
        Me.modifArtista2.Visible = False
        '
        'lblErrorModifArtista2
        '
        Me.lblErrorModifArtista2.AutoSize = True
        Me.lblErrorModifArtista2.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorModifArtista2.ForeColor = System.Drawing.Color.White
        Me.lblErrorModifArtista2.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorModifArtista2.Name = "lblErrorModifArtista2"
        Me.lblErrorModifArtista2.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorModifArtista2.TabIndex = 74
        Me.lblErrorModifArtista2.Visible = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.White
        Me.Label26.Location = New System.Drawing.Point(209, 9)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(261, 34)
        Me.Label26.TabIndex = 73
        Me.Label26.Text = "Modificar un artista"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(94, 256)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(82, 17)
        Me.Label27.TabIndex = 72
        Me.Label27.Text = "Descripción"
        '
        'txtdescripcionModifArtista2
        '
        Me.txtdescripcionModifArtista2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtdescripcionModifArtista2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtdescripcionModifArtista2.ForeColor = System.Drawing.Color.White
        Me.txtdescripcionModifArtista2.Location = New System.Drawing.Point(97, 276)
        Me.txtdescripcionModifArtista2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtdescripcionModifArtista2.Multiline = True
        Me.txtdescripcionModifArtista2.Name = "txtdescripcionModifArtista2"
        Me.txtdescripcionModifArtista2.Size = New System.Drawing.Size(438, 184)
        Me.txtdescripcionModifArtista2.TabIndex = 71
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.White
        Me.Label28.Location = New System.Drawing.Point(94, 188)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(169, 17)
        Me.Label28.TabIndex = 70
        Me.Label28.Text = "Link de la imagen de perfil"
        '
        'txtlink_imagenModifArtista2
        '
        Me.txtlink_imagenModifArtista2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtlink_imagenModifArtista2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlink_imagenModifArtista2.ForeColor = System.Drawing.Color.White
        Me.txtlink_imagenModifArtista2.Location = New System.Drawing.Point(97, 208)
        Me.txtlink_imagenModifArtista2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtlink_imagenModifArtista2.Name = "txtlink_imagenModifArtista2"
        Me.txtlink_imagenModifArtista2.Size = New System.Drawing.Size(438, 20)
        Me.txtlink_imagenModifArtista2.TabIndex = 69
        '
        'txtemailModifArtista2
        '
        Me.txtemailModifArtista2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtemailModifArtista2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtemailModifArtista2.ForeColor = System.Drawing.Color.White
        Me.txtemailModifArtista2.Location = New System.Drawing.Point(97, 138)
        Me.txtemailModifArtista2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtemailModifArtista2.Name = "txtemailModifArtista2"
        Me.txtemailModifArtista2.Size = New System.Drawing.Size(438, 20)
        Me.txtemailModifArtista2.TabIndex = 67
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.White
        Me.Label34.Location = New System.Drawing.Point(94, 117)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(42, 17)
        Me.Label34.TabIndex = 66
        Me.Label34.Text = "Email"
        '
        'txtnombreModifArtista2
        '
        Me.txtnombreModifArtista2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtnombreModifArtista2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtnombreModifArtista2.ForeColor = System.Drawing.Color.White
        Me.txtnombreModifArtista2.Location = New System.Drawing.Point(97, 73)
        Me.txtnombreModifArtista2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtnombreModifArtista2.Name = "txtnombreModifArtista2"
        Me.txtnombreModifArtista2.Size = New System.Drawing.Size(438, 20)
        Me.txtnombreModifArtista2.TabIndex = 65
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(94, 52)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(59, 17)
        Me.Label35.TabIndex = 64
        Me.Label35.Text = "Nombre"
        '
        'bttnModifArtista2
        '
        Me.bttnModifArtista2.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.bttnModifArtista2.FlatAppearance.BorderSize = 0
        Me.bttnModifArtista2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.bttnModifArtista2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttnModifArtista2.ForeColor = System.Drawing.Color.White
        Me.bttnModifArtista2.Location = New System.Drawing.Point(97, 483)
        Me.bttnModifArtista2.Name = "bttnModifArtista2"
        Me.bttnModifArtista2.Size = New System.Drawing.Size(438, 34)
        Me.bttnModifArtista2.TabIndex = 21
        Me.bttnModifArtista2.Text = "Realizar modificación"
        Me.bttnModifArtista2.UseVisualStyleBackColor = False
        '
        'modifAlbum1
        '
        Me.modifAlbum1.Controls.Add(Me.Label38)
        Me.modifAlbum1.Controls.Add(Me.Label37)
        Me.modifAlbum1.Controls.Add(Me.lvAlbumsModifAlbum1)
        Me.modifAlbum1.Controls.Add(Me.lvArtistasModifAlbum1)
        Me.modifAlbum1.Controls.Add(Me.Label33)
        Me.modifAlbum1.Controls.Add(Me.Label36)
        Me.modifAlbum1.Controls.Add(Me.Button10)
        Me.modifAlbum1.Location = New System.Drawing.Point(238, 0)
        Me.modifAlbum1.Name = "modifAlbum1"
        Me.modifAlbum1.Size = New System.Drawing.Size(646, 550)
        Me.modifAlbum1.TabIndex = 77
        Me.modifAlbum1.Visible = False
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.White
        Me.Label38.Location = New System.Drawing.Point(46, 60)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(149, 17)
        Me.Label38.TabIndex = 78
        Me.Label38.Text = "Artista autor del album"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(46, 272)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(49, 17)
        Me.Label37.TabIndex = 77
        Me.Label37.Text = "Album"
        '
        'lvAlbumsModifAlbum1
        '
        Me.lvAlbumsModifAlbum1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvAlbumsModifAlbum1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvAlbumsModifAlbum1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader28, Me.ColumnHeader29, Me.ColumnHeader30, Me.ColumnHeader31, Me.ColumnHeader32})
        Me.lvAlbumsModifAlbum1.ForeColor = System.Drawing.Color.White
        Me.lvAlbumsModifAlbum1.FullRowSelect = True
        Me.lvAlbumsModifAlbum1.HideSelection = False
        Me.lvAlbumsModifAlbum1.Location = New System.Drawing.Point(46, 292)
        Me.lvAlbumsModifAlbum1.MultiSelect = False
        Me.lvAlbumsModifAlbum1.Name = "lvAlbumsModifAlbum1"
        Me.lvAlbumsModifAlbum1.Size = New System.Drawing.Size(564, 176)
        Me.lvAlbumsModifAlbum1.TabIndex = 76
        Me.lvAlbumsModifAlbum1.UseCompatibleStateImageBehavior = False
        Me.lvAlbumsModifAlbum1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader28
        '
        Me.ColumnHeader28.Text = "ID"
        Me.ColumnHeader28.Width = 39
        '
        'ColumnHeader29
        '
        Me.ColumnHeader29.Text = "Nombre"
        Me.ColumnHeader29.Width = 179
        '
        'ColumnHeader30
        '
        Me.ColumnHeader30.Text = "Descripción"
        Me.ColumnHeader30.Width = 204
        '
        'ColumnHeader31
        '
        Me.ColumnHeader31.Text = "Subida"
        Me.ColumnHeader31.Width = 82
        '
        'ColumnHeader32
        '
        Me.ColumnHeader32.Text = "Año"
        Me.ColumnHeader32.Width = 58
        '
        'lvArtistasModifAlbum1
        '
        Me.lvArtistasModifAlbum1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvArtistasModifAlbum1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvArtistasModifAlbum1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader24, Me.ColumnHeader25, Me.ColumnHeader26, Me.ColumnHeader27})
        Me.lvArtistasModifAlbum1.ForeColor = System.Drawing.Color.White
        Me.lvArtistasModifAlbum1.FullRowSelect = True
        Me.lvArtistasModifAlbum1.HideSelection = False
        Me.lvArtistasModifAlbum1.Location = New System.Drawing.Point(46, 80)
        Me.lvArtistasModifAlbum1.MultiSelect = False
        Me.lvArtistasModifAlbum1.Name = "lvArtistasModifAlbum1"
        Me.lvArtistasModifAlbum1.Size = New System.Drawing.Size(564, 176)
        Me.lvArtistasModifAlbum1.TabIndex = 75
        Me.lvArtistasModifAlbum1.UseCompatibleStateImageBehavior = False
        Me.lvArtistasModifAlbum1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader24
        '
        Me.ColumnHeader24.Text = "ID"
        Me.ColumnHeader24.Width = 39
        '
        'ColumnHeader25
        '
        Me.ColumnHeader25.Text = "Nombre"
        Me.ColumnHeader25.Width = 108
        '
        'ColumnHeader26
        '
        Me.ColumnHeader26.Text = "Email"
        Me.ColumnHeader26.Width = 183
        '
        'ColumnHeader27
        '
        Me.ColumnHeader27.Text = "Descripción"
        Me.ColumnHeader27.Width = 234
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.White
        Me.Label33.Location = New System.Drawing.Point(264, 528)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(0, 17)
        Me.Label33.TabIndex = 74
        Me.Label33.Visible = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(123, 17)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(413, 34)
        Me.Label36.TabIndex = 73
        Me.Label36.Text = "Seleccione el album a modificar"
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button10.FlatAppearance.BorderSize = 0
        Me.Button10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.ForeColor = System.Drawing.Color.White
        Me.Button10.Location = New System.Drawing.Point(97, 483)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(438, 34)
        Me.Button10.TabIndex = 21
        Me.Button10.Text = "Modificar seleccionado"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'modifAlbum2
        '
        Me.modifAlbum2.Controls.Add(Me.lblErrorModifAlbum2)
        Me.modifAlbum2.Controls.Add(Me.Label11)
        Me.modifAlbum2.Controls.Add(Me.Label25)
        Me.modifAlbum2.Controls.Add(Me.txtDescripcionModifAlbum2)
        Me.modifAlbum2.Controls.Add(Me.Label29)
        Me.modifAlbum2.Controls.Add(Me.txtlink_imagenModifAlbum2)
        Me.modifAlbum2.Controls.Add(Me.txtaño_edicionmodifAlbum2)
        Me.modifAlbum2.Controls.Add(Me.Label30)
        Me.modifAlbum2.Controls.Add(Me.txtnombreModifAlbum2)
        Me.modifAlbum2.Controls.Add(Me.Label31)
        Me.modifAlbum2.Controls.Add(Me.Button9)
        Me.modifAlbum2.Location = New System.Drawing.Point(238, 0)
        Me.modifAlbum2.Name = "modifAlbum2"
        Me.modifAlbum2.Size = New System.Drawing.Size(646, 550)
        Me.modifAlbum2.TabIndex = 75
        Me.modifAlbum2.Visible = False
        '
        'lblErrorModifAlbum2
        '
        Me.lblErrorModifAlbum2.AutoSize = True
        Me.lblErrorModifAlbum2.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorModifAlbum2.ForeColor = System.Drawing.Color.White
        Me.lblErrorModifAlbum2.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorModifAlbum2.Name = "lblErrorModifAlbum2"
        Me.lblErrorModifAlbum2.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorModifAlbum2.TabIndex = 74
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(218, 9)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(262, 34)
        Me.Label11.TabIndex = 73
        Me.Label11.Text = "Modificar un album"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(94, 256)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(82, 17)
        Me.Label25.TabIndex = 72
        Me.Label25.Text = "Descripción"
        '
        'txtDescripcionModifAlbum2
        '
        Me.txtDescripcionModifAlbum2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtDescripcionModifAlbum2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcionModifAlbum2.ForeColor = System.Drawing.Color.White
        Me.txtDescripcionModifAlbum2.Location = New System.Drawing.Point(97, 276)
        Me.txtDescripcionModifAlbum2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtDescripcionModifAlbum2.Multiline = True
        Me.txtDescripcionModifAlbum2.Name = "txtDescripcionModifAlbum2"
        Me.txtDescripcionModifAlbum2.Size = New System.Drawing.Size(438, 184)
        Me.txtDescripcionModifAlbum2.TabIndex = 71
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.White
        Me.Label29.Location = New System.Drawing.Point(94, 188)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(169, 17)
        Me.Label29.TabIndex = 70
        Me.Label29.Text = "Link de la imagen de perfil"
        '
        'txtlink_imagenModifAlbum2
        '
        Me.txtlink_imagenModifAlbum2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtlink_imagenModifAlbum2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlink_imagenModifAlbum2.ForeColor = System.Drawing.Color.White
        Me.txtlink_imagenModifAlbum2.Location = New System.Drawing.Point(97, 208)
        Me.txtlink_imagenModifAlbum2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtlink_imagenModifAlbum2.Name = "txtlink_imagenModifAlbum2"
        Me.txtlink_imagenModifAlbum2.Size = New System.Drawing.Size(438, 20)
        Me.txtlink_imagenModifAlbum2.TabIndex = 69
        '
        'txtaño_edicionmodifAlbum2
        '
        Me.txtaño_edicionmodifAlbum2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtaño_edicionmodifAlbum2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtaño_edicionmodifAlbum2.ForeColor = System.Drawing.Color.White
        Me.txtaño_edicionmodifAlbum2.Location = New System.Drawing.Point(97, 138)
        Me.txtaño_edicionmodifAlbum2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtaño_edicionmodifAlbum2.Name = "txtaño_edicionmodifAlbum2"
        Me.txtaño_edicionmodifAlbum2.Size = New System.Drawing.Size(438, 20)
        Me.txtaño_edicionmodifAlbum2.TabIndex = 67
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(94, 117)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(179, 17)
        Me.Label30.TabIndex = 66
        Me.Label30.Text = "Año edicion (1800 - Actual)"
        '
        'txtnombreModifAlbum2
        '
        Me.txtnombreModifAlbum2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtnombreModifAlbum2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtnombreModifAlbum2.ForeColor = System.Drawing.Color.White
        Me.txtnombreModifAlbum2.Location = New System.Drawing.Point(97, 73)
        Me.txtnombreModifAlbum2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtnombreModifAlbum2.Name = "txtnombreModifAlbum2"
        Me.txtnombreModifAlbum2.Size = New System.Drawing.Size(438, 20)
        Me.txtnombreModifAlbum2.TabIndex = 65
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.White
        Me.Label31.Location = New System.Drawing.Point(94, 52)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(59, 17)
        Me.Label31.TabIndex = 64
        Me.Label31.Text = "Nombre"
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button9.FlatAppearance.BorderSize = 0
        Me.Button9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.Location = New System.Drawing.Point(97, 483)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(438, 34)
        Me.Button9.TabIndex = 21
        Me.Button9.Text = "Realizar modificación"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'subirAlbum
        '
        Me.subirAlbum.Controls.Add(Me.lvArtistasSubirAlbum)
        Me.subirAlbum.Controls.Add(Me.Label45)
        Me.subirAlbum.Controls.Add(Me.lblErrorSubirAlbum)
        Me.subirAlbum.Controls.Add(Me.Label40)
        Me.subirAlbum.Controls.Add(Me.Label41)
        Me.subirAlbum.Controls.Add(Me.txtdescripcionSubirAlbum)
        Me.subirAlbum.Controls.Add(Me.Label42)
        Me.subirAlbum.Controls.Add(Me.txtlink_imagenSubirAlbum)
        Me.subirAlbum.Controls.Add(Me.txtaño_edicionSubirAlbum)
        Me.subirAlbum.Controls.Add(Me.Label43)
        Me.subirAlbum.Controls.Add(Me.txtnombreSubirAlbum)
        Me.subirAlbum.Controls.Add(Me.Label44)
        Me.subirAlbum.Controls.Add(Me.Button11)
        Me.subirAlbum.Location = New System.Drawing.Point(238, 0)
        Me.subirAlbum.Name = "subirAlbum"
        Me.subirAlbum.Size = New System.Drawing.Size(646, 550)
        Me.subirAlbum.TabIndex = 76
        Me.subirAlbum.Visible = False
        '
        'lvArtistasSubirAlbum
        '
        Me.lvArtistasSubirAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvArtistasSubirAlbum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvArtistasSubirAlbum.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.idar, Me.ColumnHeader21, Me.ColumnHeader22})
        Me.lvArtistasSubirAlbum.ForeColor = System.Drawing.Color.White
        Me.lvArtistasSubirAlbum.FullRowSelect = True
        Me.lvArtistasSubirAlbum.HideSelection = False
        Me.lvArtistasSubirAlbum.Location = New System.Drawing.Point(95, 80)
        Me.lvArtistasSubirAlbum.MultiSelect = False
        Me.lvArtistasSubirAlbum.Name = "lvArtistasSubirAlbum"
        Me.lvArtistasSubirAlbum.Size = New System.Drawing.Size(440, 121)
        Me.lvArtistasSubirAlbum.TabIndex = 77
        Me.lvArtistasSubirAlbum.UseCompatibleStateImageBehavior = False
        Me.lvArtistasSubirAlbum.View = System.Windows.Forms.View.Details
        '
        'idar
        '
        Me.idar.Text = "ID"
        Me.idar.Width = 32
        '
        'ColumnHeader21
        '
        Me.ColumnHeader21.Text = "Nombre"
        Me.ColumnHeader21.Width = 160
        '
        'ColumnHeader22
        '
        Me.ColumnHeader22.Text = "Correo"
        Me.ColumnHeader22.Width = 247
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.White
        Me.Label45.Location = New System.Drawing.Point(94, 60)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(49, 17)
        Me.Label45.TabIndex = 76
        Me.Label45.Text = "Artista"
        '
        'lblErrorSubirAlbum
        '
        Me.lblErrorSubirAlbum.AutoSize = True
        Me.lblErrorSubirAlbum.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorSubirAlbum.ForeColor = System.Drawing.Color.White
        Me.lblErrorSubirAlbum.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorSubirAlbum.Name = "lblErrorSubirAlbum"
        Me.lblErrorSubirAlbum.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorSubirAlbum.TabIndex = 74
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(218, 9)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(206, 34)
        Me.Label40.TabIndex = 73
        Me.Label40.Text = "Subir un album"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(94, 371)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(82, 17)
        Me.Label41.TabIndex = 72
        Me.Label41.Text = "Descripción"
        '
        'txtdescripcionSubirAlbum
        '
        Me.txtdescripcionSubirAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtdescripcionSubirAlbum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtdescripcionSubirAlbum.ForeColor = System.Drawing.Color.White
        Me.txtdescripcionSubirAlbum.Location = New System.Drawing.Point(97, 391)
        Me.txtdescripcionSubirAlbum.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtdescripcionSubirAlbum.Multiline = True
        Me.txtdescripcionSubirAlbum.Name = "txtdescripcionSubirAlbum"
        Me.txtdescripcionSubirAlbum.Size = New System.Drawing.Size(439, 77)
        Me.txtdescripcionSubirAlbum.TabIndex = 71
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(92, 320)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(184, 17)
        Me.Label42.TabIndex = 70
        Me.Label42.Text = "Link de la imagen de portada"
        '
        'txtlink_imagenSubirAlbum
        '
        Me.txtlink_imagenSubirAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtlink_imagenSubirAlbum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlink_imagenSubirAlbum.ForeColor = System.Drawing.Color.White
        Me.txtlink_imagenSubirAlbum.Location = New System.Drawing.Point(95, 340)
        Me.txtlink_imagenSubirAlbum.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtlink_imagenSubirAlbum.Name = "txtlink_imagenSubirAlbum"
        Me.txtlink_imagenSubirAlbum.Size = New System.Drawing.Size(441, 20)
        Me.txtlink_imagenSubirAlbum.TabIndex = 69
        '
        'txtaño_edicionSubirAlbum
        '
        Me.txtaño_edicionSubirAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtaño_edicionSubirAlbum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtaño_edicionSubirAlbum.ForeColor = System.Drawing.Color.White
        Me.txtaño_edicionSubirAlbum.Location = New System.Drawing.Point(95, 284)
        Me.txtaño_edicionSubirAlbum.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtaño_edicionSubirAlbum.Name = "txtaño_edicionSubirAlbum"
        Me.txtaño_edicionSubirAlbum.Size = New System.Drawing.Size(440, 20)
        Me.txtaño_edicionSubirAlbum.TabIndex = 67
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.White
        Me.Label43.Location = New System.Drawing.Point(92, 264)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(197, 17)
        Me.Label43.TabIndex = 66
        Me.Label43.Text = "Año de edicion (1800 - Actual)"
        '
        'txtnombreSubirAlbum
        '
        Me.txtnombreSubirAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtnombreSubirAlbum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtnombreSubirAlbum.ForeColor = System.Drawing.Color.White
        Me.txtnombreSubirAlbum.Location = New System.Drawing.Point(95, 228)
        Me.txtnombreSubirAlbum.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtnombreSubirAlbum.Name = "txtnombreSubirAlbum"
        Me.txtnombreSubirAlbum.Size = New System.Drawing.Size(440, 20)
        Me.txtnombreSubirAlbum.TabIndex = 65
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.White
        Me.Label44.Location = New System.Drawing.Point(92, 208)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(59, 17)
        Me.Label44.TabIndex = 64
        Me.Label44.Text = "Nombre"
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button11.FlatAppearance.BorderSize = 0
        Me.Button11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.ForeColor = System.Drawing.Color.White
        Me.Button11.Location = New System.Drawing.Point(97, 483)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(438, 34)
        Me.Button11.TabIndex = 21
        Me.Button11.Text = "Subir"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'eliminarAlbum
        '
        Me.eliminarAlbum.Controls.Add(Me.Label46)
        Me.eliminarAlbum.Controls.Add(Me.Label47)
        Me.eliminarAlbum.Controls.Add(Me.lvAlbumsEliminarAlbum)
        Me.eliminarAlbum.Controls.Add(Me.lvArtistasEliminarAlbum)
        Me.eliminarAlbum.Controls.Add(Me.lblErrorEliminarAlbum)
        Me.eliminarAlbum.Controls.Add(Me.Label49)
        Me.eliminarAlbum.Controls.Add(Me.Button12)
        Me.eliminarAlbum.Location = New System.Drawing.Point(238, 0)
        Me.eliminarAlbum.Name = "eliminarAlbum"
        Me.eliminarAlbum.Size = New System.Drawing.Size(646, 550)
        Me.eliminarAlbum.TabIndex = 79
        Me.eliminarAlbum.Visible = False
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.ForeColor = System.Drawing.Color.White
        Me.Label46.Location = New System.Drawing.Point(46, 60)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(149, 17)
        Me.Label46.TabIndex = 78
        Me.Label46.Text = "Artista autor del album"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.White
        Me.Label47.Location = New System.Drawing.Point(46, 264)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(49, 17)
        Me.Label47.TabIndex = 77
        Me.Label47.Text = "Album"
        '
        'lvAlbumsEliminarAlbum
        '
        Me.lvAlbumsEliminarAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvAlbumsEliminarAlbum.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvAlbumsEliminarAlbum.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader23, Me.ColumnHeader37, Me.ColumnHeader38, Me.ColumnHeader39, Me.ColumnHeader40})
        Me.lvAlbumsEliminarAlbum.ForeColor = System.Drawing.Color.White
        Me.lvAlbumsEliminarAlbum.FullRowSelect = True
        Me.lvAlbumsEliminarAlbum.HideSelection = False
        Me.lvAlbumsEliminarAlbum.Location = New System.Drawing.Point(46, 284)
        Me.lvAlbumsEliminarAlbum.MultiSelect = False
        Me.lvAlbumsEliminarAlbum.Name = "lvAlbumsEliminarAlbum"
        Me.lvAlbumsEliminarAlbum.Size = New System.Drawing.Size(564, 176)
        Me.lvAlbumsEliminarAlbum.TabIndex = 76
        Me.lvAlbumsEliminarAlbum.UseCompatibleStateImageBehavior = False
        Me.lvAlbumsEliminarAlbum.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader23
        '
        Me.ColumnHeader23.Text = "ID"
        Me.ColumnHeader23.Width = 39
        '
        'ColumnHeader37
        '
        Me.ColumnHeader37.Text = "Nombre"
        Me.ColumnHeader37.Width = 179
        '
        'ColumnHeader38
        '
        Me.ColumnHeader38.Text = "Descripción"
        Me.ColumnHeader38.Width = 204
        '
        'ColumnHeader39
        '
        Me.ColumnHeader39.Text = "Subida"
        Me.ColumnHeader39.Width = 82
        '
        'ColumnHeader40
        '
        Me.ColumnHeader40.Text = "Año"
        Me.ColumnHeader40.Width = 58
        '
        'lvArtistasEliminarAlbum
        '
        Me.lvArtistasEliminarAlbum.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvArtistasEliminarAlbum.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvArtistasEliminarAlbum.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader41, Me.ColumnHeader42, Me.ColumnHeader43, Me.ColumnHeader44})
        Me.lvArtistasEliminarAlbum.ForeColor = System.Drawing.Color.White
        Me.lvArtistasEliminarAlbum.FullRowSelect = True
        Me.lvArtistasEliminarAlbum.HideSelection = False
        Me.lvArtistasEliminarAlbum.Location = New System.Drawing.Point(46, 80)
        Me.lvArtistasEliminarAlbum.MultiSelect = False
        Me.lvArtistasEliminarAlbum.Name = "lvArtistasEliminarAlbum"
        Me.lvArtistasEliminarAlbum.Size = New System.Drawing.Size(564, 176)
        Me.lvArtistasEliminarAlbum.TabIndex = 75
        Me.lvArtistasEliminarAlbum.UseCompatibleStateImageBehavior = False
        Me.lvArtistasEliminarAlbum.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader41
        '
        Me.ColumnHeader41.Text = "ID"
        Me.ColumnHeader41.Width = 39
        '
        'ColumnHeader42
        '
        Me.ColumnHeader42.Text = "Nombre"
        Me.ColumnHeader42.Width = 108
        '
        'ColumnHeader43
        '
        Me.ColumnHeader43.Text = "Email"
        Me.ColumnHeader43.Width = 183
        '
        'ColumnHeader44
        '
        Me.ColumnHeader44.Text = "Descripción"
        Me.ColumnHeader44.Width = 234
        '
        'lblErrorEliminarAlbum
        '
        Me.lblErrorEliminarAlbum.AutoSize = True
        Me.lblErrorEliminarAlbum.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorEliminarAlbum.ForeColor = System.Drawing.Color.White
        Me.lblErrorEliminarAlbum.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorEliminarAlbum.Name = "lblErrorEliminarAlbum"
        Me.lblErrorEliminarAlbum.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorEliminarAlbum.TabIndex = 74
        Me.lblErrorEliminarAlbum.Visible = False
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.White
        Me.Label49.Location = New System.Drawing.Point(123, 17)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(395, 34)
        Me.Label49.TabIndex = 73
        Me.Label49.Text = "Seleccione el album a eliminar"
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button12.FlatAppearance.BorderSize = 0
        Me.Button12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.ForeColor = System.Drawing.Color.White
        Me.Button12.Location = New System.Drawing.Point(97, 483)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(438, 34)
        Me.Button12.TabIndex = 21
        Me.Button12.Text = "Eliminar seleccionado"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button13.FlatAppearance.BorderSize = 0
        Me.Button13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button13.ForeColor = System.Drawing.Color.White
        Me.Button13.Location = New System.Drawing.Point(97, 483)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(438, 34)
        Me.Button13.TabIndex = 21
        Me.Button13.Text = "Eliminar seleccionado"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.ForeColor = System.Drawing.Color.White
        Me.Label53.Location = New System.Drawing.Point(210, 19)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(245, 34)
        Me.Label53.TabIndex = 73
        Me.Label53.Text = "Listado de albums"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.ForeColor = System.Drawing.Color.White
        Me.Label52.Location = New System.Drawing.Point(264, 528)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(0, 17)
        Me.Label52.TabIndex = 74
        Me.Label52.Visible = False
        '
        'lvAlbums
        '
        Me.lvAlbums.Location = New System.Drawing.Point(0, 0)
        Me.lvAlbums.Name = "lvAlbums"
        Me.lvAlbums.Size = New System.Drawing.Size(121, 97)
        Me.lvAlbums.TabIndex = 0
        Me.lvAlbums.UseCompatibleStateImageBehavior = False
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(0, 0)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(75, 23)
        Me.Button14.TabIndex = 0
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(0, 0)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(75, 23)
        Me.Button15.TabIndex = 0
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(0, 0)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(75, 23)
        Me.Button16.TabIndex = 0
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(0, 0)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(75, 23)
        Me.Button17.TabIndex = 0
        '
        'Label50
        '
        Me.Label50.Location = New System.Drawing.Point(0, 0)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(100, 23)
        Me.Label50.TabIndex = 0
        '
        'listarAlbums
        '
        Me.listarAlbums.Controls.Add(Me.lvAlbumsListarAlbums)
        Me.listarAlbums.Controls.Add(Me.Label51)
        Me.listarAlbums.Controls.Add(Me.Label54)
        Me.listarAlbums.Location = New System.Drawing.Point(238, 0)
        Me.listarAlbums.Name = "listarAlbums"
        Me.listarAlbums.Size = New System.Drawing.Size(646, 550)
        Me.listarAlbums.TabIndex = 76
        Me.listarAlbums.Visible = False
        '
        'lvAlbumsListarAlbums
        '
        Me.lvAlbumsListarAlbums.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvAlbumsListarAlbums.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvAlbumsListarAlbums.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader55, Me.ColumnHeader56, Me.ColumnHeader61, Me.ColumnHeader62, Me.ColumnHeader63, Me.ColumnHeader64})
        Me.lvAlbumsListarAlbums.ForeColor = System.Drawing.Color.White
        Me.lvAlbumsListarAlbums.FullRowSelect = True
        Me.lvAlbumsListarAlbums.HideSelection = False
        Me.lvAlbumsListarAlbums.Location = New System.Drawing.Point(46, 54)
        Me.lvAlbumsListarAlbums.Name = "lvAlbumsListarAlbums"
        Me.lvAlbumsListarAlbums.Size = New System.Drawing.Size(564, 465)
        Me.lvAlbumsListarAlbums.TabIndex = 78
        Me.lvAlbumsListarAlbums.UseCompatibleStateImageBehavior = False
        Me.lvAlbumsListarAlbums.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader55
        '
        Me.ColumnHeader55.Text = "ID"
        Me.ColumnHeader55.Width = 36
        '
        'ColumnHeader56
        '
        Me.ColumnHeader56.Text = "Artista"
        Me.ColumnHeader56.Width = 97
        '
        'ColumnHeader61
        '
        Me.ColumnHeader61.Text = "Nombre"
        Me.ColumnHeader61.Width = 134
        '
        'ColumnHeader62
        '
        Me.ColumnHeader62.Text = "Descripción"
        Me.ColumnHeader62.Width = 152
        '
        'ColumnHeader63
        '
        Me.ColumnHeader63.Text = "Subida"
        Me.ColumnHeader63.Width = 84
        '
        'ColumnHeader64
        '
        Me.ColumnHeader64.Text = "Año"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.ForeColor = System.Drawing.Color.White
        Me.Label51.Location = New System.Drawing.Point(264, 528)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(0, 17)
        Me.Label51.TabIndex = 74
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(215, 9)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(245, 34)
        Me.Label54.TabIndex = 73
        Me.Label54.Text = "Listado de albums"
        '
        'subirCancion
        '
        Me.subirCancion.Controls.Add(Me.txtlink_recursoSubirCancion)
        Me.subirCancion.Controls.Add(Me.Label62)
        Me.subirCancion.Controls.Add(Me.txtlink_imagenSubirCancion)
        Me.subirCancion.Controls.Add(Me.Label61)
        Me.subirCancion.Controls.Add(Me.txtNombreSubirCancion)
        Me.subirCancion.Controls.Add(Me.Label60)
        Me.subirCancion.Controls.Add(Me.lbGenerosSubirCancion)
        Me.subirCancion.Controls.Add(Me.Label59)
        Me.subirCancion.Controls.Add(Me.Label55)
        Me.subirCancion.Controls.Add(Me.Label56)
        Me.subirCancion.Controls.Add(Me.lvAlbumsSubirCancion)
        Me.subirCancion.Controls.Add(Me.lvArtistasSubirCancion)
        Me.subirCancion.Controls.Add(Me.lblErrorSubirCancion)
        Me.subirCancion.Controls.Add(Me.Label58)
        Me.subirCancion.Controls.Add(Me.Button18)
        Me.subirCancion.Location = New System.Drawing.Point(238, 0)
        Me.subirCancion.Name = "subirCancion"
        Me.subirCancion.Size = New System.Drawing.Size(791, 550)
        Me.subirCancion.TabIndex = 80
        Me.subirCancion.Visible = False
        '
        'txtlink_recursoSubirCancion
        '
        Me.txtlink_recursoSubirCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtlink_recursoSubirCancion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlink_recursoSubirCancion.ForeColor = System.Drawing.Color.White
        Me.txtlink_recursoSubirCancion.Location = New System.Drawing.Point(417, 430)
        Me.txtlink_recursoSubirCancion.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtlink_recursoSubirCancion.Name = "txtlink_recursoSubirCancion"
        Me.txtlink_recursoSubirCancion.Size = New System.Drawing.Size(334, 20)
        Me.txtlink_recursoSubirCancion.TabIndex = 86
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.ForeColor = System.Drawing.Color.White
        Me.Label62.Location = New System.Drawing.Point(414, 409)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(105, 17)
        Me.Label62.TabIndex = 85
        Me.Label62.Text = "Link del recurso"
        '
        'txtlink_imagenSubirCancion
        '
        Me.txtlink_imagenSubirCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtlink_imagenSubirCancion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlink_imagenSubirCancion.ForeColor = System.Drawing.Color.White
        Me.txtlink_imagenSubirCancion.Location = New System.Drawing.Point(417, 366)
        Me.txtlink_imagenSubirCancion.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtlink_imagenSubirCancion.Name = "txtlink_imagenSubirCancion"
        Me.txtlink_imagenSubirCancion.Size = New System.Drawing.Size(334, 20)
        Me.txtlink_imagenSubirCancion.TabIndex = 84
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.ForeColor = System.Drawing.Color.White
        Me.Label61.Location = New System.Drawing.Point(414, 345)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(184, 17)
        Me.Label61.TabIndex = 83
        Me.Label61.Text = "Link de la imagen de portada"
        '
        'txtNombreSubirCancion
        '
        Me.txtNombreSubirCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtNombreSubirCancion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombreSubirCancion.ForeColor = System.Drawing.Color.White
        Me.txtNombreSubirCancion.Location = New System.Drawing.Point(417, 305)
        Me.txtNombreSubirCancion.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtNombreSubirCancion.Name = "txtNombreSubirCancion"
        Me.txtNombreSubirCancion.Size = New System.Drawing.Size(334, 20)
        Me.txtNombreSubirCancion.TabIndex = 82
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.ForeColor = System.Drawing.Color.White
        Me.Label60.Location = New System.Drawing.Point(414, 284)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(59, 17)
        Me.Label60.TabIndex = 81
        Me.Label60.Text = "Nombre"
        '
        'lbGenerosSubirCancion
        '
        Me.lbGenerosSubirCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lbGenerosSubirCancion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbGenerosSubirCancion.ForeColor = System.Drawing.Color.White
        Me.lbGenerosSubirCancion.FormattingEnabled = True
        Me.lbGenerosSubirCancion.Location = New System.Drawing.Point(49, 297)
        Me.lbGenerosSubirCancion.Name = "lbGenerosSubirCancion"
        Me.lbGenerosSubirCancion.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lbGenerosSubirCancion.Size = New System.Drawing.Size(331, 158)
        Me.lbGenerosSubirCancion.TabIndex = 80
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.White
        Me.Label59.Location = New System.Drawing.Point(46, 276)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(149, 17)
        Me.Label59.TabIndex = 79
        Me.Label59.Text = "Genero/s de la canción"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.ForeColor = System.Drawing.Color.White
        Me.Label55.Location = New System.Drawing.Point(46, 60)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(169, 17)
        Me.Label55.TabIndex = 78
        Me.Label55.Text = "Artista autor de la canción"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.White
        Me.Label56.Location = New System.Drawing.Point(417, 61)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(133, 17)
        Me.Label56.TabIndex = 77
        Me.Label56.Text = "Album de la canción"
        '
        'lvAlbumsSubirCancion
        '
        Me.lvAlbumsSubirCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvAlbumsSubirCancion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvAlbumsSubirCancion.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader53, Me.ColumnHeader54, Me.ColumnHeader57})
        Me.lvAlbumsSubirCancion.ForeColor = System.Drawing.Color.White
        Me.lvAlbumsSubirCancion.FullRowSelect = True
        Me.lvAlbumsSubirCancion.HideSelection = False
        Me.lvAlbumsSubirCancion.Location = New System.Drawing.Point(417, 80)
        Me.lvAlbumsSubirCancion.MultiSelect = False
        Me.lvAlbumsSubirCancion.Name = "lvAlbumsSubirCancion"
        Me.lvAlbumsSubirCancion.Size = New System.Drawing.Size(334, 176)
        Me.lvAlbumsSubirCancion.TabIndex = 76
        Me.lvAlbumsSubirCancion.UseCompatibleStateImageBehavior = False
        Me.lvAlbumsSubirCancion.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader53
        '
        Me.ColumnHeader53.Text = "ID"
        Me.ColumnHeader53.Width = 39
        '
        'ColumnHeader54
        '
        Me.ColumnHeader54.Text = "Nombre"
        Me.ColumnHeader54.Width = 198
        '
        'ColumnHeader57
        '
        Me.ColumnHeader57.Text = "Año"
        Me.ColumnHeader57.Width = 91
        '
        'lvArtistasSubirCancion
        '
        Me.lvArtistasSubirCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvArtistasSubirCancion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvArtistasSubirCancion.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader58, Me.ColumnHeader59, Me.ColumnHeader60})
        Me.lvArtistasSubirCancion.ForeColor = System.Drawing.Color.White
        Me.lvArtistasSubirCancion.FullRowSelect = True
        Me.lvArtistasSubirCancion.HideSelection = False
        Me.lvArtistasSubirCancion.Location = New System.Drawing.Point(46, 80)
        Me.lvArtistasSubirCancion.MultiSelect = False
        Me.lvArtistasSubirCancion.Name = "lvArtistasSubirCancion"
        Me.lvArtistasSubirCancion.Size = New System.Drawing.Size(334, 176)
        Me.lvArtistasSubirCancion.TabIndex = 75
        Me.lvArtistasSubirCancion.UseCompatibleStateImageBehavior = False
        Me.lvArtistasSubirCancion.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader58
        '
        Me.ColumnHeader58.Text = "ID"
        Me.ColumnHeader58.Width = 39
        '
        'ColumnHeader59
        '
        Me.ColumnHeader59.Text = "Nombre"
        Me.ColumnHeader59.Width = 125
        '
        'ColumnHeader60
        '
        Me.ColumnHeader60.Text = "Email"
        Me.ColumnHeader60.Width = 170
        '
        'lblErrorSubirCancion
        '
        Me.lblErrorSubirCancion.AutoSize = True
        Me.lblErrorSubirCancion.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorSubirCancion.ForeColor = System.Drawing.Color.White
        Me.lblErrorSubirCancion.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorSubirCancion.Name = "lblErrorSubirCancion"
        Me.lblErrorSubirCancion.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorSubirCancion.TabIndex = 74
        Me.lblErrorSubirCancion.Visible = False
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.White
        Me.Label58.Location = New System.Drawing.Point(228, 19)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(238, 34)
        Me.Label58.TabIndex = 73
        Me.Label58.Text = "Subir una canción"
        '
        'Button18
        '
        Me.Button18.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button18.FlatAppearance.BorderSize = 0
        Me.Button18.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button18.ForeColor = System.Drawing.Color.White
        Me.Button18.Location = New System.Drawing.Point(97, 483)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(608, 34)
        Me.Button18.TabIndex = 21
        Me.Button18.Text = "Subir canción"
        Me.Button18.UseVisualStyleBackColor = False
        '
        'listarCanciones
        '
        Me.listarCanciones.Controls.Add(Me.lvCancionesListarCanciones)
        Me.listarCanciones.Controls.Add(Me.Label9)
        Me.listarCanciones.Controls.Add(Me.Label32)
        Me.listarCanciones.Location = New System.Drawing.Point(238, 0)
        Me.listarCanciones.Name = "listarCanciones"
        Me.listarCanciones.Size = New System.Drawing.Size(646, 550)
        Me.listarCanciones.TabIndex = 79
        Me.listarCanciones.Visible = False
        '
        'lvCancionesListarCanciones
        '
        Me.lvCancionesListarCanciones.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvCancionesListarCanciones.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvCancionesListarCanciones.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader51, Me.ColumnHeader52, Me.ColumnHeader65, Me.ColumnHeader68, Me.ColumnHeader69, Me.ColumnHeader70})
        Me.lvCancionesListarCanciones.ForeColor = System.Drawing.Color.White
        Me.lvCancionesListarCanciones.FullRowSelect = True
        Me.lvCancionesListarCanciones.HideSelection = False
        Me.lvCancionesListarCanciones.Location = New System.Drawing.Point(46, 54)
        Me.lvCancionesListarCanciones.Name = "lvCancionesListarCanciones"
        Me.lvCancionesListarCanciones.Size = New System.Drawing.Size(564, 465)
        Me.lvCancionesListarCanciones.TabIndex = 78
        Me.lvCancionesListarCanciones.UseCompatibleStateImageBehavior = False
        Me.lvCancionesListarCanciones.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader51
        '
        Me.ColumnHeader51.Text = "ID"
        Me.ColumnHeader51.Width = 33
        '
        'ColumnHeader52
        '
        Me.ColumnHeader52.Text = "Artista"
        Me.ColumnHeader52.Width = 78
        '
        'ColumnHeader65
        '
        Me.ColumnHeader65.Text = "Album"
        Me.ColumnHeader65.Width = 75
        '
        'ColumnHeader68
        '
        Me.ColumnHeader68.Text = "Nombre"
        Me.ColumnHeader68.Width = 99
        '
        'ColumnHeader69
        '
        Me.ColumnHeader69.Text = "Imagen"
        Me.ColumnHeader69.Width = 131
        '
        'ColumnHeader70
        '
        Me.ColumnHeader70.Text = "Recurso"
        Me.ColumnHeader70.Width = 146
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(264, 528)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(0, 17)
        Me.Label9.TabIndex = 74
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.White
        Me.Label32.Location = New System.Drawing.Point(215, 9)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(277, 34)
        Me.Label32.TabIndex = 73
        Me.Label32.Text = "Listado de canciones"
        '
        'eliminarCancion
        '
        Me.eliminarCancion.Controls.Add(Me.Label57)
        Me.eliminarCancion.Controls.Add(Me.lvCancionesEliminarCancion)
        Me.eliminarCancion.Controls.Add(Me.Label66)
        Me.eliminarCancion.Controls.Add(Me.Label67)
        Me.eliminarCancion.Controls.Add(Me.lvAlbumsEliminarCancion)
        Me.eliminarCancion.Controls.Add(Me.lvArtistasEliminarCancion)
        Me.eliminarCancion.Controls.Add(Me.lblErrorEliminarCancion)
        Me.eliminarCancion.Controls.Add(Me.Label69)
        Me.eliminarCancion.Controls.Add(Me.Button19)
        Me.eliminarCancion.Location = New System.Drawing.Point(238, 0)
        Me.eliminarCancion.Name = "eliminarCancion"
        Me.eliminarCancion.Size = New System.Drawing.Size(791, 550)
        Me.eliminarCancion.TabIndex = 87
        Me.eliminarCancion.Visible = False
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.White
        Me.Label57.Location = New System.Drawing.Point(43, 259)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(59, 17)
        Me.Label57.TabIndex = 80
        Me.Label57.Text = "Canción"
        '
        'lvCancionesEliminarCancion
        '
        Me.lvCancionesEliminarCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvCancionesEliminarCancion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvCancionesEliminarCancion.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader72, Me.ColumnHeader75, Me.ColumnHeader76, Me.ColumnHeader77})
        Me.lvCancionesEliminarCancion.ForeColor = System.Drawing.Color.White
        Me.lvCancionesEliminarCancion.FullRowSelect = True
        Me.lvCancionesEliminarCancion.HideSelection = False
        Me.lvCancionesEliminarCancion.Location = New System.Drawing.Point(46, 276)
        Me.lvCancionesEliminarCancion.MultiSelect = False
        Me.lvCancionesEliminarCancion.Name = "lvCancionesEliminarCancion"
        Me.lvCancionesEliminarCancion.Size = New System.Drawing.Size(705, 192)
        Me.lvCancionesEliminarCancion.TabIndex = 79
        Me.lvCancionesEliminarCancion.UseCompatibleStateImageBehavior = False
        Me.lvCancionesEliminarCancion.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader72
        '
        Me.ColumnHeader72.Text = "ID"
        Me.ColumnHeader72.Width = 33
        '
        'ColumnHeader75
        '
        Me.ColumnHeader75.Text = "Nombre"
        Me.ColumnHeader75.Width = 127
        '
        'ColumnHeader76
        '
        Me.ColumnHeader76.Text = "Imagen"
        Me.ColumnHeader76.Width = 231
        '
        'ColumnHeader77
        '
        Me.ColumnHeader77.Text = "Recurso"
        Me.ColumnHeader77.Width = 313
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.White
        Me.Label66.Location = New System.Drawing.Point(46, 60)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(169, 17)
        Me.Label66.TabIndex = 78
        Me.Label66.Text = "Artista autor de la canción"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.ForeColor = System.Drawing.Color.White
        Me.Label67.Location = New System.Drawing.Point(417, 61)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(133, 17)
        Me.Label67.TabIndex = 77
        Me.Label67.Text = "Album de la canción"
        '
        'lvAlbumsEliminarCancion
        '
        Me.lvAlbumsEliminarCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvAlbumsEliminarCancion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvAlbumsEliminarCancion.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader48, Me.ColumnHeader49, Me.ColumnHeader50})
        Me.lvAlbumsEliminarCancion.ForeColor = System.Drawing.Color.White
        Me.lvAlbumsEliminarCancion.FullRowSelect = True
        Me.lvAlbumsEliminarCancion.HideSelection = False
        Me.lvAlbumsEliminarCancion.Location = New System.Drawing.Point(417, 80)
        Me.lvAlbumsEliminarCancion.MultiSelect = False
        Me.lvAlbumsEliminarCancion.Name = "lvAlbumsEliminarCancion"
        Me.lvAlbumsEliminarCancion.Size = New System.Drawing.Size(334, 176)
        Me.lvAlbumsEliminarCancion.TabIndex = 76
        Me.lvAlbumsEliminarCancion.UseCompatibleStateImageBehavior = False
        Me.lvAlbumsEliminarCancion.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader48
        '
        Me.ColumnHeader48.Text = "ID"
        Me.ColumnHeader48.Width = 39
        '
        'ColumnHeader49
        '
        Me.ColumnHeader49.Text = "Nombre"
        Me.ColumnHeader49.Width = 198
        '
        'ColumnHeader50
        '
        Me.ColumnHeader50.Text = "Año"
        Me.ColumnHeader50.Width = 91
        '
        'lvArtistasEliminarCancion
        '
        Me.lvArtistasEliminarCancion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvArtistasEliminarCancion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvArtistasEliminarCancion.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader66, Me.ColumnHeader67, Me.ColumnHeader71})
        Me.lvArtistasEliminarCancion.ForeColor = System.Drawing.Color.White
        Me.lvArtistasEliminarCancion.FullRowSelect = True
        Me.lvArtistasEliminarCancion.HideSelection = False
        Me.lvArtistasEliminarCancion.Location = New System.Drawing.Point(46, 80)
        Me.lvArtistasEliminarCancion.MultiSelect = False
        Me.lvArtistasEliminarCancion.Name = "lvArtistasEliminarCancion"
        Me.lvArtistasEliminarCancion.Size = New System.Drawing.Size(334, 176)
        Me.lvArtistasEliminarCancion.TabIndex = 75
        Me.lvArtistasEliminarCancion.UseCompatibleStateImageBehavior = False
        Me.lvArtistasEliminarCancion.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader66
        '
        Me.ColumnHeader66.Text = "ID"
        Me.ColumnHeader66.Width = 39
        '
        'ColumnHeader67
        '
        Me.ColumnHeader67.Text = "Nombre"
        Me.ColumnHeader67.Width = 125
        '
        'ColumnHeader71
        '
        Me.ColumnHeader71.Text = "Email"
        Me.ColumnHeader71.Width = 170
        '
        'lblErrorEliminarCancion
        '
        Me.lblErrorEliminarCancion.AutoSize = True
        Me.lblErrorEliminarCancion.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorEliminarCancion.ForeColor = System.Drawing.Color.White
        Me.lblErrorEliminarCancion.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorEliminarCancion.Name = "lblErrorEliminarCancion"
        Me.lblErrorEliminarCancion.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorEliminarCancion.TabIndex = 74
        Me.lblErrorEliminarCancion.Visible = False
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.ForeColor = System.Drawing.Color.White
        Me.Label69.Location = New System.Drawing.Point(285, 19)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(275, 34)
        Me.Label69.TabIndex = 73
        Me.Label69.Text = "Eliminar una canción"
        '
        'Button19
        '
        Me.Button19.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button19.FlatAppearance.BorderSize = 0
        Me.Button19.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button19.ForeColor = System.Drawing.Color.White
        Me.Button19.Location = New System.Drawing.Point(97, 483)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(608, 34)
        Me.Button19.TabIndex = 21
        Me.Button19.Text = "Eliminar canción"
        Me.Button19.UseVisualStyleBackColor = False
        '
        'modifCancion1
        '
        Me.modifCancion1.Controls.Add(Me.Label63)
        Me.modifCancion1.Controls.Add(Me.lvCancionesModifCancion1)
        Me.modifCancion1.Controls.Add(Me.Label64)
        Me.modifCancion1.Controls.Add(Me.Label65)
        Me.modifCancion1.Controls.Add(Me.lvAlbumsModifCancion1)
        Me.modifCancion1.Controls.Add(Me.lvArtistasModifCancion1)
        Me.modifCancion1.Controls.Add(Me.lblErrorModifCancion1)
        Me.modifCancion1.Controls.Add(Me.Label70)
        Me.modifCancion1.Controls.Add(Me.Button20)
        Me.modifCancion1.Location = New System.Drawing.Point(238, 0)
        Me.modifCancion1.Name = "modifCancion1"
        Me.modifCancion1.Size = New System.Drawing.Size(791, 550)
        Me.modifCancion1.TabIndex = 88
        Me.modifCancion1.Visible = False
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.ForeColor = System.Drawing.Color.White
        Me.Label63.Location = New System.Drawing.Point(43, 259)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(59, 17)
        Me.Label63.TabIndex = 80
        Me.Label63.Text = "Canción"
        '
        'lvCancionesModifCancion1
        '
        Me.lvCancionesModifCancion1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvCancionesModifCancion1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvCancionesModifCancion1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader73, Me.ColumnHeader74, Me.ColumnHeader78, Me.ColumnHeader79})
        Me.lvCancionesModifCancion1.ForeColor = System.Drawing.Color.White
        Me.lvCancionesModifCancion1.FullRowSelect = True
        Me.lvCancionesModifCancion1.HideSelection = False
        Me.lvCancionesModifCancion1.Location = New System.Drawing.Point(46, 276)
        Me.lvCancionesModifCancion1.MultiSelect = False
        Me.lvCancionesModifCancion1.Name = "lvCancionesModifCancion1"
        Me.lvCancionesModifCancion1.Size = New System.Drawing.Size(705, 192)
        Me.lvCancionesModifCancion1.TabIndex = 79
        Me.lvCancionesModifCancion1.UseCompatibleStateImageBehavior = False
        Me.lvCancionesModifCancion1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader73
        '
        Me.ColumnHeader73.Text = "ID"
        Me.ColumnHeader73.Width = 33
        '
        'ColumnHeader74
        '
        Me.ColumnHeader74.Text = "Nombre"
        Me.ColumnHeader74.Width = 127
        '
        'ColumnHeader78
        '
        Me.ColumnHeader78.Text = "Imagen"
        Me.ColumnHeader78.Width = 231
        '
        'ColumnHeader79
        '
        Me.ColumnHeader79.Text = "Recurso"
        Me.ColumnHeader79.Width = 313
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(46, 60)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(169, 17)
        Me.Label64.TabIndex = 78
        Me.Label64.Text = "Artista autor de la canción"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.Color.White
        Me.Label65.Location = New System.Drawing.Point(417, 61)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(133, 17)
        Me.Label65.TabIndex = 77
        Me.Label65.Text = "Album de la canción"
        '
        'lvAlbumsModifCancion1
        '
        Me.lvAlbumsModifCancion1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvAlbumsModifCancion1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvAlbumsModifCancion1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader80, Me.ColumnHeader81, Me.ColumnHeader82})
        Me.lvAlbumsModifCancion1.ForeColor = System.Drawing.Color.White
        Me.lvAlbumsModifCancion1.FullRowSelect = True
        Me.lvAlbumsModifCancion1.HideSelection = False
        Me.lvAlbumsModifCancion1.Location = New System.Drawing.Point(417, 80)
        Me.lvAlbumsModifCancion1.MultiSelect = False
        Me.lvAlbumsModifCancion1.Name = "lvAlbumsModifCancion1"
        Me.lvAlbumsModifCancion1.Size = New System.Drawing.Size(334, 176)
        Me.lvAlbumsModifCancion1.TabIndex = 76
        Me.lvAlbumsModifCancion1.UseCompatibleStateImageBehavior = False
        Me.lvAlbumsModifCancion1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader80
        '
        Me.ColumnHeader80.Text = "ID"
        Me.ColumnHeader80.Width = 39
        '
        'ColumnHeader81
        '
        Me.ColumnHeader81.Text = "Nombre"
        Me.ColumnHeader81.Width = 198
        '
        'ColumnHeader82
        '
        Me.ColumnHeader82.Text = "Año"
        Me.ColumnHeader82.Width = 91
        '
        'lvArtistasModifCancion1
        '
        Me.lvArtistasModifCancion1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lvArtistasModifCancion1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lvArtistasModifCancion1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader83, Me.ColumnHeader84, Me.ColumnHeader85})
        Me.lvArtistasModifCancion1.ForeColor = System.Drawing.Color.White
        Me.lvArtistasModifCancion1.FullRowSelect = True
        Me.lvArtistasModifCancion1.HideSelection = False
        Me.lvArtistasModifCancion1.Location = New System.Drawing.Point(46, 80)
        Me.lvArtistasModifCancion1.MultiSelect = False
        Me.lvArtistasModifCancion1.Name = "lvArtistasModifCancion1"
        Me.lvArtistasModifCancion1.Size = New System.Drawing.Size(334, 176)
        Me.lvArtistasModifCancion1.TabIndex = 75
        Me.lvArtistasModifCancion1.UseCompatibleStateImageBehavior = False
        Me.lvArtistasModifCancion1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader83
        '
        Me.ColumnHeader83.Text = "ID"
        Me.ColumnHeader83.Width = 39
        '
        'ColumnHeader84
        '
        Me.ColumnHeader84.Text = "Nombre"
        Me.ColumnHeader84.Width = 125
        '
        'ColumnHeader85
        '
        Me.ColumnHeader85.Text = "Email"
        Me.ColumnHeader85.Width = 170
        '
        'lblErrorModifCancion1
        '
        Me.lblErrorModifCancion1.AutoSize = True
        Me.lblErrorModifCancion1.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorModifCancion1.ForeColor = System.Drawing.Color.White
        Me.lblErrorModifCancion1.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorModifCancion1.Name = "lblErrorModifCancion1"
        Me.lblErrorModifCancion1.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorModifCancion1.TabIndex = 74
        Me.lblErrorModifCancion1.Visible = False
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.ForeColor = System.Drawing.Color.White
        Me.Label70.Location = New System.Drawing.Point(285, 19)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(294, 34)
        Me.Label70.TabIndex = 73
        Me.Label70.Text = "Modificar una canción"
        '
        'Button20
        '
        Me.Button20.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button20.FlatAppearance.BorderSize = 0
        Me.Button20.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button20.ForeColor = System.Drawing.Color.White
        Me.Button20.Location = New System.Drawing.Point(97, 483)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(608, 34)
        Me.Button20.TabIndex = 21
        Me.Button20.Text = "Modificar canción"
        Me.Button20.UseVisualStyleBackColor = False
        '
        'modifCancion2
        '
        Me.modifCancion2.Controls.Add(Me.lbGenerosModifCancion2)
        Me.modifCancion2.Controls.Add(Me.Label68)
        Me.modifCancion2.Controls.Add(Me.lblErrorModifCancion2)
        Me.modifCancion2.Controls.Add(Me.Label72)
        Me.modifCancion2.Controls.Add(Me.Label74)
        Me.modifCancion2.Controls.Add(Me.txtnombreModifCancion2)
        Me.modifCancion2.Controls.Add(Me.txtlink_recursoModifCancion2)
        Me.modifCancion2.Controls.Add(Me.Label75)
        Me.modifCancion2.Controls.Add(Me.txtlink_imagenModifCancion2)
        Me.modifCancion2.Controls.Add(Me.Label76)
        Me.modifCancion2.Controls.Add(Me.Button21)
        Me.modifCancion2.Location = New System.Drawing.Point(238, 0)
        Me.modifCancion2.Name = "modifCancion2"
        Me.modifCancion2.Size = New System.Drawing.Size(646, 550)
        Me.modifCancion2.TabIndex = 78
        Me.modifCancion2.Visible = False
        '
        'lbGenerosModifCancion2
        '
        Me.lbGenerosModifCancion2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lbGenerosModifCancion2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbGenerosModifCancion2.ForeColor = System.Drawing.Color.White
        Me.lbGenerosModifCancion2.FormattingEnabled = True
        Me.lbGenerosModifCancion2.Location = New System.Drawing.Point(118, 86)
        Me.lbGenerosModifCancion2.Name = "lbGenerosModifCancion2"
        Me.lbGenerosModifCancion2.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lbGenerosModifCancion2.Size = New System.Drawing.Size(442, 210)
        Me.lbGenerosModifCancion2.TabIndex = 77
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.ForeColor = System.Drawing.Color.White
        Me.Label68.Location = New System.Drawing.Point(117, 64)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(65, 17)
        Me.Label68.TabIndex = 76
        Me.Label68.Text = "Genero/s"
        '
        'lblErrorModifCancion2
        '
        Me.lblErrorModifCancion2.AutoSize = True
        Me.lblErrorModifCancion2.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorModifCancion2.ForeColor = System.Drawing.Color.White
        Me.lblErrorModifCancion2.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorModifCancion2.Name = "lblErrorModifCancion2"
        Me.lblErrorModifCancion2.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorModifCancion2.TabIndex = 74
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label72.ForeColor = System.Drawing.Color.White
        Me.Label72.Location = New System.Drawing.Point(198, 17)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(294, 34)
        Me.Label72.TabIndex = 73
        Me.Label72.Text = "Modificar una canción"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label74.ForeColor = System.Drawing.Color.White
        Me.Label74.Location = New System.Drawing.Point(115, 308)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(59, 17)
        Me.Label74.TabIndex = 70
        Me.Label74.Text = "Nombre"
        '
        'txtnombreModifCancion2
        '
        Me.txtnombreModifCancion2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtnombreModifCancion2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtnombreModifCancion2.ForeColor = System.Drawing.Color.White
        Me.txtnombreModifCancion2.Location = New System.Drawing.Point(118, 328)
        Me.txtnombreModifCancion2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtnombreModifCancion2.Name = "txtnombreModifCancion2"
        Me.txtnombreModifCancion2.Size = New System.Drawing.Size(441, 20)
        Me.txtnombreModifCancion2.TabIndex = 69
        '
        'txtlink_recursoModifCancion2
        '
        Me.txtlink_recursoModifCancion2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtlink_recursoModifCancion2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlink_recursoModifCancion2.ForeColor = System.Drawing.Color.White
        Me.txtlink_recursoModifCancion2.Location = New System.Drawing.Point(120, 435)
        Me.txtlink_recursoModifCancion2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtlink_recursoModifCancion2.Name = "txtlink_recursoModifCancion2"
        Me.txtlink_recursoModifCancion2.Size = New System.Drawing.Size(440, 20)
        Me.txtlink_recursoModifCancion2.TabIndex = 67
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.ForeColor = System.Drawing.Color.White
        Me.Label75.Location = New System.Drawing.Point(117, 415)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(189, 17)
        Me.Label75.TabIndex = 66
        Me.Label75.Text = "Link del recurso de la canción"
        '
        'txtlink_imagenModifCancion2
        '
        Me.txtlink_imagenModifCancion2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtlink_imagenModifCancion2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlink_imagenModifCancion2.ForeColor = System.Drawing.Color.White
        Me.txtlink_imagenModifCancion2.Location = New System.Drawing.Point(120, 379)
        Me.txtlink_imagenModifCancion2.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtlink_imagenModifCancion2.Name = "txtlink_imagenModifCancion2"
        Me.txtlink_imagenModifCancion2.Size = New System.Drawing.Size(440, 20)
        Me.txtlink_imagenModifCancion2.TabIndex = 65
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label76.ForeColor = System.Drawing.Color.White
        Me.Label76.Location = New System.Drawing.Point(117, 359)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(184, 17)
        Me.Label76.TabIndex = 64
        Me.Label76.Text = "Link de la imagen de portada"
        '
        'Button21
        '
        Me.Button21.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button21.FlatAppearance.BorderSize = 0
        Me.Button21.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button21.ForeColor = System.Drawing.Color.White
        Me.Button21.Location = New System.Drawing.Point(122, 483)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(438, 34)
        Me.Button21.TabIndex = 21
        Me.Button21.Text = "Realizar modificación"
        Me.Button21.UseVisualStyleBackColor = False
        '
        'subirGenero
        '
        Me.subirGenero.Controls.Add(Me.lbGenerosSubirGenero)
        Me.subirGenero.Controls.Add(Me.Label71)
        Me.subirGenero.Controls.Add(Me.lblErrorSubirGenero)
        Me.subirGenero.Controls.Add(Me.Label77)
        Me.subirGenero.Controls.Add(Me.txtnombreSubirGenero)
        Me.subirGenero.Controls.Add(Me.Label79)
        Me.subirGenero.Controls.Add(Me.Button22)
        Me.subirGenero.Location = New System.Drawing.Point(238, 0)
        Me.subirGenero.Name = "subirGenero"
        Me.subirGenero.Size = New System.Drawing.Size(646, 550)
        Me.subirGenero.TabIndex = 79
        Me.subirGenero.Visible = False
        '
        'lbGenerosSubirGenero
        '
        Me.lbGenerosSubirGenero.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lbGenerosSubirGenero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbGenerosSubirGenero.ForeColor = System.Drawing.Color.White
        Me.lbGenerosSubirGenero.FormattingEnabled = True
        Me.lbGenerosSubirGenero.Location = New System.Drawing.Point(118, 86)
        Me.lbGenerosSubirGenero.Name = "lbGenerosSubirGenero"
        Me.lbGenerosSubirGenero.Size = New System.Drawing.Size(442, 314)
        Me.lbGenerosSubirGenero.TabIndex = 77
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.ForeColor = System.Drawing.Color.White
        Me.Label71.Location = New System.Drawing.Point(117, 64)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(124, 17)
        Me.Label71.TabIndex = 76
        Me.Label71.Text = "Generos existentes"
        '
        'lblErrorSubirGenero
        '
        Me.lblErrorSubirGenero.AutoSize = True
        Me.lblErrorSubirGenero.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorSubirGenero.ForeColor = System.Drawing.Color.White
        Me.lblErrorSubirGenero.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorSubirGenero.Name = "lblErrorSubirGenero"
        Me.lblErrorSubirGenero.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorSubirGenero.TabIndex = 74
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label77.ForeColor = System.Drawing.Color.White
        Me.Label77.Location = New System.Drawing.Point(238, 9)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(219, 34)
        Me.Label77.TabIndex = 73
        Me.Label77.Text = "Crear un genero"
        '
        'txtnombreSubirGenero
        '
        Me.txtnombreSubirGenero.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.txtnombreSubirGenero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtnombreSubirGenero.ForeColor = System.Drawing.Color.White
        Me.txtnombreSubirGenero.Location = New System.Drawing.Point(120, 435)
        Me.txtnombreSubirGenero.MinimumSize = New System.Drawing.Size(2, 25)
        Me.txtnombreSubirGenero.Name = "txtnombreSubirGenero"
        Me.txtnombreSubirGenero.Size = New System.Drawing.Size(440, 20)
        Me.txtnombreSubirGenero.TabIndex = 67
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.ForeColor = System.Drawing.Color.White
        Me.Label79.Location = New System.Drawing.Point(117, 415)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(59, 17)
        Me.Label79.TabIndex = 66
        Me.Label79.Text = "Nombre"
        '
        'Button22
        '
        Me.Button22.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button22.FlatAppearance.BorderSize = 0
        Me.Button22.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button22.ForeColor = System.Drawing.Color.White
        Me.Button22.Location = New System.Drawing.Point(122, 483)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(438, 34)
        Me.Button22.TabIndex = 21
        Me.Button22.Text = "Subir genero"
        Me.Button22.UseVisualStyleBackColor = False
        '
        'eliminarGenero
        '
        Me.eliminarGenero.Controls.Add(Me.lbGenerosEliminarGenero)
        Me.eliminarGenero.Controls.Add(Me.Label73)
        Me.eliminarGenero.Controls.Add(Me.lblErrorEliminarGenero)
        Me.eliminarGenero.Controls.Add(Me.Label80)
        Me.eliminarGenero.Controls.Add(Me.Button23)
        Me.eliminarGenero.Location = New System.Drawing.Point(238, 0)
        Me.eliminarGenero.Name = "eliminarGenero"
        Me.eliminarGenero.Size = New System.Drawing.Size(646, 550)
        Me.eliminarGenero.TabIndex = 80
        Me.eliminarGenero.Visible = False
        '
        'lbGenerosEliminarGenero
        '
        Me.lbGenerosEliminarGenero.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.lbGenerosEliminarGenero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbGenerosEliminarGenero.ForeColor = System.Drawing.Color.White
        Me.lbGenerosEliminarGenero.FormattingEnabled = True
        Me.lbGenerosEliminarGenero.Location = New System.Drawing.Point(118, 94)
        Me.lbGenerosEliminarGenero.Name = "lbGenerosEliminarGenero"
        Me.lbGenerosEliminarGenero.Size = New System.Drawing.Size(442, 366)
        Me.lbGenerosEliminarGenero.TabIndex = 77
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.ForeColor = System.Drawing.Color.White
        Me.Label73.Location = New System.Drawing.Point(115, 72)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(124, 17)
        Me.Label73.TabIndex = 76
        Me.Label73.Text = "Generos existentes"
        '
        'lblErrorEliminarGenero
        '
        Me.lblErrorEliminarGenero.AutoSize = True
        Me.lblErrorEliminarGenero.Font = New System.Drawing.Font("Microsoft JhengHei", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorEliminarGenero.ForeColor = System.Drawing.Color.White
        Me.lblErrorEliminarGenero.Location = New System.Drawing.Point(264, 528)
        Me.lblErrorEliminarGenero.Name = "lblErrorEliminarGenero"
        Me.lblErrorEliminarGenero.Size = New System.Drawing.Size(0, 17)
        Me.lblErrorEliminarGenero.TabIndex = 74
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Font = New System.Drawing.Font("Microsoft JhengHei", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label80.ForeColor = System.Drawing.Color.White
        Me.Label80.Location = New System.Drawing.Point(221, 19)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(253, 34)
        Me.Label80.TabIndex = 73
        Me.Label80.Text = "Eliminar un genero"
        '
        'Button23
        '
        Me.Button23.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.Button23.FlatAppearance.BorderSize = 0
        Me.Button23.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(190, Byte), Integer))
        Me.Button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button23.ForeColor = System.Drawing.Color.White
        Me.Button23.Location = New System.Drawing.Point(122, 483)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(438, 34)
        Me.Button23.TabIndex = 21
        Me.Button23.Text = "Subir genero"
        Me.Button23.UseVisualStyleBackColor = False
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(13, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(79, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(883, 550)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.eliminarGenero)
        Me.Controls.Add(Me.subirGenero)
        Me.Controls.Add(Me.modifCancion2)
        Me.Controls.Add(Me.subirAlbum)
        Me.Controls.Add(Me.modifAlbum2)
        Me.Controls.Add(Me.subirArtista)
        Me.Controls.Add(Me.generoOpciones)
        Me.Controls.Add(Me.artistaOpciones)
        Me.Controls.Add(Me.albumOpciones)
        Me.Controls.Add(Me.modifCancion1)
        Me.Controls.Add(Me.eliminarCancion)
        Me.Controls.Add(Me.cancionOpciones)
        Me.Controls.Add(Me.listarCanciones)
        Me.Controls.Add(Me.listarAlbums)
        Me.Controls.Add(Me.eliminarArtista)
        Me.Controls.Add(Me.modifArtista1)
        Me.Controls.Add(Me.modifArtista2)
        Me.Controls.Add(Me.listarArtistas)
        Me.Controls.Add(Me.subirCancion)
        Me.Controls.Add(Me.eliminarAlbum)
        Me.Controls.Add(Me.modifAlbum1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Main"
        Me.Text = "Animúsica Admin"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.artistaOpciones.ResumeLayout(False)
        Me.artistaOpciones.PerformLayout()
        Me.albumOpciones.ResumeLayout(False)
        Me.albumOpciones.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.cancionOpciones.ResumeLayout(False)
        Me.cancionOpciones.PerformLayout()
        Me.generoOpciones.ResumeLayout(False)
        Me.generoOpciones.PerformLayout()
        Me.subirArtista.ResumeLayout(False)
        Me.subirArtista.PerformLayout()
        Me.eliminarArtista.ResumeLayout(False)
        Me.eliminarArtista.PerformLayout()
        Me.listarArtistas.ResumeLayout(False)
        Me.listarArtistas.PerformLayout()
        Me.modifArtista1.ResumeLayout(False)
        Me.modifArtista1.PerformLayout()
        Me.modifArtista2.ResumeLayout(False)
        Me.modifArtista2.PerformLayout()
        Me.modifAlbum1.ResumeLayout(False)
        Me.modifAlbum1.PerformLayout()
        Me.modifAlbum2.ResumeLayout(False)
        Me.modifAlbum2.PerformLayout()
        Me.subirAlbum.ResumeLayout(False)
        Me.subirAlbum.PerformLayout()
        Me.eliminarAlbum.ResumeLayout(False)
        Me.eliminarAlbum.PerformLayout()
        Me.listarAlbums.ResumeLayout(False)
        Me.listarAlbums.PerformLayout()
        Me.subirCancion.ResumeLayout(False)
        Me.subirCancion.PerformLayout()
        Me.listarCanciones.ResumeLayout(False)
        Me.listarCanciones.PerformLayout()
        Me.eliminarCancion.ResumeLayout(False)
        Me.eliminarCancion.PerformLayout()
        Me.modifCancion1.ResumeLayout(False)
        Me.modifCancion1.PerformLayout()
        Me.modifCancion2.ResumeLayout(False)
        Me.modifCancion2.PerformLayout()
        Me.subirGenero.ResumeLayout(False)
        Me.subirGenero.PerformLayout()
        Me.eliminarGenero.ResumeLayout(False)
        Me.eliminarGenero.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents artistaOpciones As Panel
    Friend WithEvents lblSelección As Label
    Friend WithEvents bttnListarArtistas As Button
    Friend WithEvents bttnEliminarArtista As Button
    Friend WithEvents bttnModifArtista As Button
    Friend WithEvents bttnCrearArtista As Button
    Friend WithEvents albumOpciones As Panel
    Friend WithEvents bttnListarAlbums As Button
    Friend WithEvents bttnEliminarAlbum As Button
    Friend WithEvents bttnModifAlbum As Button
    Friend WithEvents bttnCrearAlbum As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents cancionOpciones As Panel
    Friend WithEvents bttnListarCanciones As Button
    Friend WithEvents bttnEliminarCancion As Button
    Friend WithEvents bttnModifCancion As Button
    Friend WithEvents bttnCrearCancion As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents generoOpciones As Panel
    Friend WithEvents bttnCrearGenero As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents subirArtista As Panel
    Friend WithEvents Button5 As Button
    Friend WithEvents txtNombreSubirArtista As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtEmailSubirArtista As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtlink_imagenSubirArtista As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtDescripcionSubirArtista As TextBox
    Friend WithEvents lblErrorSubirArtista As Label
    Friend WithEvents eliminarArtista As Panel
    Friend WithEvents lblErrorEliminarArtista As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents bttnEliminar As Button
    Friend WithEvents lvArtistas As ListView
    Friend WithEvents ID As ColumnHeader
    Friend WithEvents Nombre As ColumnHeader
    Friend WithEvents Descripcion As ColumnHeader
    Friend WithEvents Correo As ColumnHeader
    Friend WithEvents listarArtistas As Panel
    Friend WithEvents lvArtistasl As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents Label12 As Label
    Friend WithEvents modifArtista1 As Panel
    Friend WithEvents lvArtistasModif1 As ListView
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ColumnHeader8 As ColumnHeader
    Friend WithEvents lblErrorModifArtista1 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents bttnModif1 As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Button8 As Button
    Friend WithEvents modifArtista2 As Panel
    Friend WithEvents lblErrorModifArtista2 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents txtdescripcionModifArtista2 As TextBox
    Friend WithEvents Label28 As Label
    Friend WithEvents txtlink_imagenModifArtista2 As TextBox
    Friend WithEvents txtemailModifArtista2 As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents txtnombreModifArtista2 As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents bttnModifArtista2 As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents ListView1 As ListView
    Friend WithEvents ColumnHeader9 As ColumnHeader
    Friend WithEvents ColumnHeader10 As ColumnHeader
    Friend WithEvents ColumnHeader11 As ColumnHeader
    Friend WithEvents ColumnHeader12 As ColumnHeader
    Friend WithEvents Label18 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents ListView2 As ListView
    Friend WithEvents ColumnHeader13 As ColumnHeader
    Friend WithEvents ColumnHeader14 As ColumnHeader
    Friend WithEvents ColumnHeader15 As ColumnHeader
    Friend WithEvents ColumnHeader16 As ColumnHeader
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Button6 As Button
    Friend WithEvents Panel4 As Panel
    Friend WithEvents ListView3 As ListView
    Friend WithEvents ColumnHeader17 As ColumnHeader
    Friend WithEvents ColumnHeader18 As ColumnHeader
    Friend WithEvents ColumnHeader19 As ColumnHeader
    Friend WithEvents ColumnHeader20 As ColumnHeader
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Button7 As Button
    Friend WithEvents modifAlbum1 As Panel
    Friend WithEvents lvArtistasModifAlbum1 As ListView
    Friend WithEvents ColumnHeader24 As ColumnHeader
    Friend WithEvents ColumnHeader25 As ColumnHeader
    Friend WithEvents ColumnHeader26 As ColumnHeader
    Friend WithEvents ColumnHeader27 As ColumnHeader
    Friend WithEvents Label33 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents Button10 As Button
    Friend WithEvents Label38 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents lvAlbumsModifAlbum1 As ListView
    Friend WithEvents ColumnHeader28 As ColumnHeader
    Friend WithEvents ColumnHeader29 As ColumnHeader
    Friend WithEvents ColumnHeader30 As ColumnHeader
    Friend WithEvents ColumnHeader31 As ColumnHeader
    Friend WithEvents ColumnHeader32 As ColumnHeader
    Friend WithEvents modifAlbum2 As Panel
    Friend WithEvents lblErrorModifAlbum2 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents txtDescripcionModifAlbum2 As TextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents txtlink_imagenModifAlbum2 As TextBox
    Friend WithEvents txtaño_edicionmodifAlbum2 As TextBox
    Friend WithEvents Label30 As Label
    Friend WithEvents txtnombreModifAlbum2 As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents Button9 As Button
    Friend WithEvents ListView4 As ListView
    Friend WithEvents ColumnHeader33 As ColumnHeader
    Friend WithEvents ColumnHeader34 As ColumnHeader
    Friend WithEvents ColumnHeader35 As ColumnHeader
    Friend WithEvents ColumnHeader36 As ColumnHeader
    Friend WithEvents Label39 As Label
    Friend WithEvents subirAlbum As Panel
    Friend WithEvents lblErrorSubirAlbum As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents txtdescripcionSubirAlbum As TextBox
    Friend WithEvents Label42 As Label
    Friend WithEvents txtlink_imagenSubirAlbum As TextBox
    Friend WithEvents txtaño_edicionSubirAlbum As TextBox
    Friend WithEvents Label43 As Label
    Friend WithEvents txtnombreSubirAlbum As TextBox
    Friend WithEvents Label44 As Label
    Friend WithEvents Button11 As Button
    Friend WithEvents Label45 As Label
    Friend WithEvents lvArtistasSubirAlbum As ListView
    Friend WithEvents idar As ColumnHeader
    Friend WithEvents ColumnHeader21 As ColumnHeader
    Friend WithEvents ColumnHeader22 As ColumnHeader
    Friend WithEvents eliminarAlbum As Panel
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents lvAlbumsEliminarAlbum As ListView
    Friend WithEvents ColumnHeader23 As ColumnHeader
    Friend WithEvents ColumnHeader37 As ColumnHeader
    Friend WithEvents ColumnHeader38 As ColumnHeader
    Friend WithEvents ColumnHeader39 As ColumnHeader
    Friend WithEvents ColumnHeader40 As ColumnHeader
    Friend WithEvents lvArtistasEliminarAlbum As ListView
    Friend WithEvents ColumnHeader41 As ColumnHeader
    Friend WithEvents ColumnHeader42 As ColumnHeader
    Friend WithEvents ColumnHeader43 As ColumnHeader
    Friend WithEvents ColumnHeader44 As ColumnHeader
    Friend WithEvents lblErrorEliminarAlbum As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Button12 As Button
    Friend WithEvents lvAlbums As ListView
    Friend WithEvents ColumnHeader45 As ColumnHeader
    Friend WithEvents Artista As ColumnHeader
    Friend WithEvents ColumnHeader46 As ColumnHeader
    Friend WithEvents Año As ColumnHeader
    Friend WithEvents ColumnHeader47 As ColumnHeader
    Friend WithEvents Subido As ColumnHeader
    Friend WithEvents Label52 As Label
    Friend WithEvents Label53 As Label
    Friend WithEvents Button13 As Button
    Friend WithEvents Button14 As Button
    Friend WithEvents Button15 As Button
    Friend WithEvents Button16 As Button
    Friend WithEvents Button17 As Button
    Friend WithEvents Label50 As Label
    Friend WithEvents listarAlbums As Panel
    Friend WithEvents lvAlbumsListarAlbums As ListView
    Friend WithEvents Label51 As Label
    Friend WithEvents Label54 As Label
    Friend WithEvents subirCancion As Panel
    Friend WithEvents Label55 As Label
    Friend WithEvents Label56 As Label
    Friend WithEvents lvAlbumsSubirCancion As ListView
    Friend WithEvents ColumnHeader53 As ColumnHeader
    Friend WithEvents ColumnHeader54 As ColumnHeader
    Friend WithEvents ColumnHeader57 As ColumnHeader
    Friend WithEvents lvArtistasSubirCancion As ListView
    Friend WithEvents ColumnHeader58 As ColumnHeader
    Friend WithEvents ColumnHeader59 As ColumnHeader
    Friend WithEvents ColumnHeader60 As ColumnHeader
    Friend WithEvents lblErrorSubirCancion As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Button18 As Button
    Friend WithEvents lbGenerosSubirCancion As ListBox
    Friend WithEvents Label59 As Label
    Friend WithEvents txtlink_recursoSubirCancion As TextBox
    Friend WithEvents Label62 As Label
    Friend WithEvents txtlink_imagenSubirCancion As TextBox
    Friend WithEvents Label61 As Label
    Friend WithEvents txtNombreSubirCancion As TextBox
    Friend WithEvents Label60 As Label
    Friend WithEvents ColumnHeader55 As ColumnHeader
    Friend WithEvents ColumnHeader56 As ColumnHeader
    Friend WithEvents ColumnHeader61 As ColumnHeader
    Friend WithEvents ColumnHeader62 As ColumnHeader
    Friend WithEvents ColumnHeader63 As ColumnHeader
    Friend WithEvents ColumnHeader64 As ColumnHeader
    Friend WithEvents listarCanciones As Panel
    Friend WithEvents lvCancionesListarCanciones As ListView
    Friend WithEvents Label9 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents ColumnHeader51 As ColumnHeader
    Friend WithEvents ColumnHeader52 As ColumnHeader
    Friend WithEvents ColumnHeader65 As ColumnHeader
    Friend WithEvents ColumnHeader68 As ColumnHeader
    Friend WithEvents ColumnHeader69 As ColumnHeader
    Friend WithEvents ColumnHeader70 As ColumnHeader
    Friend WithEvents eliminarCancion As Panel
    Friend WithEvents Label66 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents lvAlbumsEliminarCancion As ListView
    Friend WithEvents ColumnHeader48 As ColumnHeader
    Friend WithEvents ColumnHeader49 As ColumnHeader
    Friend WithEvents ColumnHeader50 As ColumnHeader
    Friend WithEvents lvArtistasEliminarCancion As ListView
    Friend WithEvents ColumnHeader66 As ColumnHeader
    Friend WithEvents ColumnHeader67 As ColumnHeader
    Friend WithEvents ColumnHeader71 As ColumnHeader
    Friend WithEvents lblErrorEliminarCancion As Label
    Friend WithEvents Label69 As Label
    Friend WithEvents Button19 As Button
    Friend WithEvents Label57 As Label
    Friend WithEvents lvCancionesEliminarCancion As ListView
    Friend WithEvents ColumnHeader72 As ColumnHeader
    Friend WithEvents ColumnHeader75 As ColumnHeader
    Friend WithEvents ColumnHeader76 As ColumnHeader
    Friend WithEvents ColumnHeader77 As ColumnHeader
    Friend WithEvents modifCancion1 As Panel
    Friend WithEvents Label63 As Label
    Friend WithEvents lvCancionesModifCancion1 As ListView
    Friend WithEvents ColumnHeader73 As ColumnHeader
    Friend WithEvents ColumnHeader74 As ColumnHeader
    Friend WithEvents ColumnHeader78 As ColumnHeader
    Friend WithEvents ColumnHeader79 As ColumnHeader
    Friend WithEvents Label64 As Label
    Friend WithEvents Label65 As Label
    Friend WithEvents lvAlbumsModifCancion1 As ListView
    Friend WithEvents ColumnHeader80 As ColumnHeader
    Friend WithEvents ColumnHeader81 As ColumnHeader
    Friend WithEvents ColumnHeader82 As ColumnHeader
    Friend WithEvents lvArtistasModifCancion1 As ListView
    Friend WithEvents ColumnHeader83 As ColumnHeader
    Friend WithEvents ColumnHeader84 As ColumnHeader
    Friend WithEvents ColumnHeader85 As ColumnHeader
    Friend WithEvents lblErrorModifCancion1 As Label
    Friend WithEvents Label70 As Label
    Friend WithEvents Button20 As Button
    Friend WithEvents modifCancion2 As Panel
    Friend WithEvents Label68 As Label
    Friend WithEvents lblErrorModifCancion2 As Label
    Friend WithEvents Label72 As Label
    Friend WithEvents txtlink_recursoModifCancion2 As TextBox
    Friend WithEvents Label75 As Label
    Friend WithEvents Button21 As Button
    Friend WithEvents lbGenerosModifCancion2 As ListBox
    Friend WithEvents Label74 As Label
    Friend WithEvents txtnombreModifCancion2 As TextBox
    Friend WithEvents txtlink_imagenModifCancion2 As TextBox
    Friend WithEvents Label76 As Label
    Friend WithEvents subirGenero As Panel
    Friend WithEvents lbGenerosSubirGenero As ListBox
    Friend WithEvents Label71 As Label
    Friend WithEvents lblErrorSubirGenero As Label
    Friend WithEvents Label77 As Label
    Friend WithEvents Button22 As Button
    Friend WithEvents txtnombreSubirGenero As TextBox
    Friend WithEvents Label79 As Label
    Friend WithEvents bttnEliminarGenero As Button
    Friend WithEvents eliminarGenero As Panel
    Friend WithEvents lbGenerosEliminarGenero As ListBox
    Friend WithEvents Label73 As Label
    Friend WithEvents lblErrorEliminarGenero As Label
    Friend WithEvents Label80 As Label
    Friend WithEvents Button23 As Button
End Class
