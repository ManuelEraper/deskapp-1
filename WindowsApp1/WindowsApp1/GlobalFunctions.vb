﻿Imports System.Reflection
Imports System.Text.RegularExpressions
Imports Npgsql

Module GlobalFunctions
    Public Sub listarListBox(listBox As ListBox, query As String)
        Dim clasecnn = New ConnectionPgSql
        Dim conexion As New Npgsql.NpgsqlConnection()
        conexion = clasecnn.AbrirConexion()

        Dim cmd = New Npgsql.NpgsqlCommand()
        cmd.Connection = conexion

        listBox.Items.Clear()

        cmd.CommandText = query
        Dim lector As NpgsqlDataReader
        lector = cmd.ExecuteReader()

        If lector.HasRows Then
            While lector.Read()
                listBox.Items.Add(lector(0).ToString)
            End While
        End If
        lector.Dispose()
    End Sub

    Public Sub listarListView(listView As ListView, query As String)
        Dim clasecnn = New ConnectionPgSql
        Dim conexion As New Npgsql.NpgsqlConnection()
        conexion = clasecnn.AbrirConexion()

        Dim cmd = New Npgsql.NpgsqlCommand()
        cmd.Connection = conexion

        listView.Items.Clear()
        cmd.CommandText = query

        Dim lector As NpgsqlDataReader
        lector = cmd.ExecuteReader()

        If lector.HasRows Then
            Dim I As Integer = 0
            While lector.Read()
                listView.Items.Add(lector(0).ToString)
                For rep As Integer = 1 To listView.Columns.Count - 1
                    listView.Items(I).SubItems.Add(lector(rep).ToString)
                Next
                I += 1
            End While
        End If
        lector.Dispose()
    End Sub
    Public Sub subir(view As Panel, lblError As Label, ParamArray objetos() As Object)
        Try
            For Each objeto In objetos
                Dim clasecnn = New ConnectionPgSql
                Dim conexion As New Npgsql.NpgsqlConnection()
                conexion = clasecnn.AbrirConexion()

                Dim cmd = New Npgsql.NpgsqlCommand()
                cmd.Connection = conexion

                Dim parametros() As PropertyInfo = objeto.GetType().GetProperties()
                If verificarInputs(view, objetos) = True Then
                    For Each PropertyInfo In parametros
                        Try
                            If PropertyInfo.Name.Substring(0, 2).Contains("id") = False Then
                                If PropertyInfo.PropertyType.ToString = "System.String" Then
                                    PropertyInfo.SetValue(objeto, view.Controls("txt" + PropertyInfo.Name + view.Name).Text)
                                ElseIf PropertyInfo.PropertyType.ToString = "System.Int32" Then
                                    PropertyInfo.SetValue(objeto, Convert.ToInt32(view.Controls("txt" + PropertyInfo.Name + view.Name).Text))
                                End If
                            End If
                        Catch ex As Exception
                        End Try
                    Next
                    agregarParametros(objeto, cmd, parametros)
                    For Each PropertyInfo In parametros
                        If PropertyInfo.Name.Substring(0, 2).Contains("id") And PropertyInfo.Name <> "id" And PropertyInfo.GetValue(objeto) = Nothing Then
                            cmd.CommandText = "Select id FROM " + PropertyInfo.Name.Substring(3) + " WHERE id = (SELECT max(id) FROM " + PropertyInfo.Name.Substring(3) + ")"
                            cmd.Parameters.Item(PropertyInfo.Name).Value = cmd.ExecuteScalar
                        End If
                    Next
                    agregarCadenaComando(objeto, cmd, parametros)
                End If
            Next
            lblError.Text = "Alta exitosa"
            lblError.Visible = True
            For Each elemento As TextBox In view.Controls.OfType(Of TextBox)
                elemento.Text = ""
            Next
        Catch ex As Exception
            lblError.Text = "Error en el alta: " + ex.Message
        lblError.Visible = True
        End Try
    End Sub
    Private Sub agregarParametros(objeto As Object, cmd As Npgsql.NpgsqlCommand, parametros As PropertyInfo())
        For Each PropertyInfo In parametros
            Select Case PropertyInfo.PropertyType.ToString
                Case "System.String"
                    cmd.Parameters.Add((PropertyInfo.Name), NpgsqlTypes.NpgsqlDbType.Varchar).Value = PropertyInfo.GetValue(objeto).ToString
                Case "System.Int32"
                    cmd.Parameters.Add((PropertyInfo.Name), NpgsqlTypes.NpgsqlDbType.Integer).Value = Convert.ToInt32(PropertyInfo.GetValue(objeto))
                Case "System.DateTime"
                    cmd.Parameters.Add((PropertyInfo.Name), NpgsqlTypes.NpgsqlDbType.Date).Value = PropertyInfo.GetValue(objeto)
            End Select
        Next
    End Sub
    Private Sub agregarCadenaComando(objeto As Object, cmd As Npgsql.NpgsqlCommand, parametros As PropertyInfo())
        Dim nombres As Integer = 0
        Dim valores As Integer = 0
        cmd.CommandText = "INSERT INTO " + objeto.GetType.Name() + "("
        For Each PropertyInfo In parametros
            If (PropertyInfo Is parametros.Last) Then
                If PropertyInfo.Name = "id" Then
                    cmd.CommandText = cmd.CommandText.Remove(cmd.CommandText.Length - 1) + ")" + " VALUES ("
                Else
                    cmd.CommandText = cmd.CommandText + cmd.Parameters.Item(nombres).ParameterName + ")" + " VALUES ("
                End If
                For Each valor As PropertyInfo In parametros
                    If (valor Is parametros.Last) Then
                        If PropertyInfo.Name = "id" Then
                            cmd.CommandText = cmd.CommandText.Remove(cmd.CommandText.Length - 1) + ");"
                        Else
                            cmd.CommandText = cmd.CommandText + "'" + cmd.Parameters.Item(valores).Value.ToString + "');"
                        End If
                    Else
                        cmd.CommandText = cmd.CommandText + "'" + cmd.Parameters.Item(valores).Value.ToString + "'" + ","
                    End If
                    valores += 1
                Next
            Else
                cmd.CommandText = cmd.CommandText + cmd.Parameters.Item(nombres).ParameterName + ","
            End If
            nombres += 1
        Next
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
    End Sub
    Private Sub crearAPartirDeId(view As Panel, id As String, ParamArray objetos() As Object)
        For Each objeto In objetos
            Dim clasecnn = New ConnectionPgSql
            Dim conexion As New Npgsql.NpgsqlConnection()
            conexion = clasecnn.AbrirConexion()
            Dim cmd = New Npgsql.NpgsqlCommand()
            cmd.Connection = conexion
            cmd.CommandText = "SELECT "

            Dim parametros() As PropertyInfo = objeto.GetType().GetProperties()
            For Each PropertyInfo In parametros
                If PropertyInfo Is parametros.Last Then
                    cmd.CommandText = cmd.CommandText + PropertyInfo.Name + " FROM " + objeto.GetType().Name + " WHERE " + PropertyInfo.Name + " = " + id
                Else
                    cmd.CommandText = cmd.CommandText + PropertyInfo.Name + ", "
                End If
            Next
            Dim lector As NpgsqlDataReader
            lector = cmd.ExecuteReader()
            Dim repeticiones As Integer = 0

            If lector.HasRows Then
                While lector.Read() And repeticiones < parametros.Length
                    For Each PropertyInfo In parametros
                        Try
                            Dim aDate As Date = Convert.ToDateTime((lector(repeticiones)))
                            Dim dateAString = aDate.ToString("MM-dd-yyyy")
                            PropertyInfo.SetValue(objeto, dateAString)
                        Catch ex As Exception
                            PropertyInfo.SetValue(objeto, lector(repeticiones))
                        End Try
                        repeticiones += 1
                    Next
                End While
            End If
            lector.Dispose()
        Next
    End Sub
    Public Sub mostrarModificado(view As Panel, id As Integer, ParamArray objetos() As Object)
        For Each objeto In objetos
            crearAPartirDeId(view, id, objeto)
            Dim parametros() As PropertyInfo = objeto.GetType().GetProperties()
            For Each PropertyInfo In parametros
                If PropertyInfo.Name.Contains("id") = False Then
                    view.Controls("txt" + PropertyInfo.Name + view.Name).Text = PropertyInfo.GetValue(objeto).ToString
                End If
            Next
        Next
    End Sub
    Public Sub modificar(view As Panel, lblError As Label, ParamArray objetos() As Object)
        lblError.Text = ""
        Try
            For Each objeto In objetos
                If verificarInputs(view, objeto) Then
                    Dim clasecnn = New ConnectionPgSql
                    Dim conexion As New Npgsql.NpgsqlConnection()
                    conexion = clasecnn.AbrirConexion()

                    Dim cmd = New Npgsql.NpgsqlCommand()
                    cmd.Connection = conexion
                    Dim parametros() As PropertyInfo = objeto.GetType().GetProperties()

                    For Each PropertyInfo In parametros
                        If PropertyInfo.Name.Substring(0, 2).Contains("id") = False And PropertyInfo.Name <> "fecha_subida" Then
                            If PropertyInfo.PropertyType.ToString = "System.Int32" Then
                                PropertyInfo.SetValue(objeto, Convert.ToInt32(view.Controls("txt" + PropertyInfo.Name + view.Name).Text))
                            Else
                                PropertyInfo.SetValue(objeto, view.Controls("txt" + PropertyInfo.Name + view.Name).Text)
                            End If
                        End If
                    Next
                    cmd.CommandText = "UPDATE " + objeto.GetType.Name() + " SET "
                    For Each PropertyInfo In parametros
                        If PropertyInfo Is parametros.Last = False And PropertyInfo.Name.Substring(0, 2).Contains("id") = False And parametros.Length > 2 Then
                            cmd.CommandText = cmd.CommandText + PropertyInfo.Name + " = '" + PropertyInfo.GetValue(objeto).ToString + "',"
                        ElseIf PropertyInfo Is parametros.First And parametros.Length = 2 Then
                            cmd.CommandText = cmd.CommandText + PropertyInfo.Name + " = '" + PropertyInfo.GetValue(objeto).ToString + "' "
                        ElseIf PropertyInfo Is parametros.Last Then
                            cmd.CommandText = cmd.CommandText.Remove(cmd.CommandText.Length - 1) + " WHERE " + PropertyInfo.Name.Substring(0) + " = " + PropertyInfo.GetValue(objeto).ToString

                        End If
                    Next
                    cmd.ExecuteNonQuery()
                End If
            Next
            lblError.Text = "Modificado con éxito"
            lblError.Visible = True
            For Each elemento As TextBox In view.Controls.OfType(Of TextBox)
                elemento.Text = ""
            Next
        Catch ex As Exception
            lblError.Text = "Error en la modificación: " + ex.Message
            lblError.Visible = True
        End Try
    End Sub

    Public Function verificarInputs(view As Panel, ParamArray objetos() As Object) As Boolean
        For Each objeto In objetos
            Dim parametros() As PropertyInfo = objeto.GetType().GetProperties()

            For Each PropertyInfo In parametros
                If view.Contains(view.Controls("txt" + PropertyInfo.Name + view.Name)) Then
                    Select Case PropertyInfo.PropertyType.ToString
                        Case "System.Int32"
                            Try
                                Convert.ToInt32(view.Controls("txt" + PropertyInfo.Name + view.Name).Text)
                                If (Convert.ToInt32(view.Controls("txt" + PropertyInfo.Name + view.Name).Text) > 1800 And Convert.ToInt32(view.Controls("txt" + PropertyInfo.Name + view.Name).Text) < Today.Year) = False Then
                                    view.Controls("lblError" + view.Name).Text = "Valor no admitido para: " + objeto.traducir(PropertyInfo)
                                    view.Controls("lblError" + view.Name).Visible() = True
                                    Return False
                                    Exit Function
                                Else
                                    view.Controls("lblError" + view.Name).Visible() = False
                                    verificarInputs = True
                                End If
                            Catch ex As Exception
                                view.Controls("lblError" + view.Name).Text = "Error en: " + objeto.traducir(PropertyInfo)
                                view.Controls("lblError" + view.Name).Visible() = True
                                Return False
                                Exit Function
                            End Try
                        Case "System.String"

                            If view.Controls("txt" + PropertyInfo.Name + view.Name).Text.Equals("") = True Then
                                view.Controls("lblError" + view.Name).Text = "No pueden quedar campos vacíos: " + objeto.traducir(PropertyInfo)
                                view.Controls("lblError" + view.Name).Visible() = True
                                Return False
                                Exit Function
                            Else
                                verificarInputs = True
                                view.Controls("lblError" + view.Name).Visible() = False
                            End If
                        Case "System.Date"
                    End Select
                Else
                    verificarInputs = True
                End If
            Next
        Next
    End Function
    Public Sub eliminar(lblError As Label, query As String)
        Try
            Dim clasecnn = New ConnectionPgSql
            Dim conexion As New Npgsql.NpgsqlConnection()
            conexion = clasecnn.AbrirConexion()

            Dim cmd = New Npgsql.NpgsqlCommand()
            cmd.Connection = conexion

            cmd.CommandText = query
            cmd.ExecuteNonQuery()

            lblError.Text = "Baja exitosa"
            lblError.Visible = True
        Catch ex As Exception
            lblError.Text = "Error en la baja: " + ex.Message
            lblError.Visible = True
        End Try

    End Sub
End Module
